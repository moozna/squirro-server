module.exports = {
    expressPort: 5000,
    mongodbUri: 'mongodb://localhost:27017/',
    dbName: {
        mock: 'squirro-mock',
        test: 'squirro-mock-test'
    }
};
