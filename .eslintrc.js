module.exports = {
    env: {
        es6: true,
        node: true,
        mocha: true
    },
    extends: 'airbnb-base',
    parserOptions: {
        ecmaVersion: 2017
    },
    rules: {
        'arrow-parens': ['error', 'as-needed', {
            requireForBlockBody: false,
        }],
        'comma-dangle': ['error', 'never'],
        'indent': ['error', 4, {
            SwitchCase: 1,
            VariableDeclarator: 1,
            outerIIFEBody: 1,
            FunctionDeclaration: {
                parameters: 1,
                body: 1
            },
            FunctionExpression: {
                parameters: 1,
                body: 1
            },
            CallExpression: {
                arguments: 1
            },
            ArrayExpression: 1,
            ObjectExpression: 1,
            ImportDeclaration: 1,
            flatTernaryExpressions: false,
            ignoredNodes: ['JSXElement', 'JSXElement *']
        }],
        'linebreak-style': ['error', 'windows'],
        'no-console': 'off',
        'no-plusplus': 'off',
        'no-use-before-define': ['error', {
            functions: false,
            classes: true,
            variables: true
        }],
    }
};
