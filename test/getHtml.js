const fs = require('fs');
const path = require('path');
const { promisify } = require('util');

const readFile = promisify(fs.readFile);
const testFolder = './test-pages';

const createPath = filename =>
    path.resolve(__dirname, testFolder, `${filename}.html`);

const getHtml = filename => readFile(createPath(filename));

module.exports = getHtml;
