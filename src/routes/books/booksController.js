const fetch = require("node-fetch");
const debug = require('debug')('squirro-server:booksController');
const db = require('../../db/db');
const {
    filteringQuery,
    suggestionQuery,
    PROJECTION,
    sortParameter,
    isUsedByOtherBookQuery,
    queryById,
    updateQuery
} = require('./queries');
const getGoodreadsData = require('./import/getGoodreadsData');
const downloadListopiaTags = require('./import/downloadListopiaTags');
const getAmazonData = require('./import/getAmazonData');
const { removeEmptyFields } = require('./bookData/createBookData');
const { deleteAllCovers } = require('../images/imagesController');

const HEADERS = {
    'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0'
};

async function getBooks(req, res) {
    const { sort = 'title', order = 1, startIndex = 0, limit = 50 } = req.query;

    const query = filteringQuery(req.query);
    const sorting = sortParameter(sort, order);

    const cursor = await db.getCollection().find(query, { projection: PROJECTION })
        .skip(startIndex)
        .limit(limit)
        .collation({ locale: "en" })
        .sort(sorting);

    const books = await cursor.toArray();
    const count = await cursor.count();
    const hasNext = startIndex + limit < count;

    res.json({ books, count, hasNext });
}

async function getSuggestions(req, res) {
    const { search } = req.query;

    const query = filteringQuery(req.query);

    const suggestions = await db.getCollection().aggregate([
            { $match: query },
            {
                $facet: {
                    titles: suggestionQuery('title', search),
                    authors: [
                        { $unwind: '$author' },
                        ...suggestionQuery('author', search)
                    ],
                    seriesTitles: suggestionQuery('seriesTitle', search)
                }
            }
        ])
        .toArray();

    res.json(suggestions[0]);
}

async function validateAsinIsNotUsed(req, res) {
    const { id } = req.params;
    const { bookId } = req.query;

    const query = isUsedByOtherBookQuery('asin', id, bookId);
    const result = await db.getCollection().find(query).count();

    res.json(!result > 0);
}

async function validateGoodreadsIdIsNotUsed(req, res) {
    const { id } = req.params;
    const { bookId } = req.query;

    const parsedId = parseInt(id);
    const query = isUsedByOtherBookQuery('goodreadsId', parsedId, bookId);
    const result = await db.getCollection().find(query).count();

    res.json(!result > 0);
}

async function importAmazonBook(req, res) {
    const { id } = req.params;
    const url = `https://www.amazon.com/dp/${id}`;

    const response = await fetch(url, { headers: HEADERS });
    const html = await response.text();
    const amazonData = getAmazonData(html);

    res.json(amazonData);
}

async function importGoodreadsBook(req, res) {
    const { id } = req.params;
    const url = `https://www.goodreads.com/book/show/${id}`;

    const response = await fetch(url);
    const html = await response.text();
    const goodreadsData = await getGoodreadsData(html);
    goodreadsData.goodreadsId = Number(id);
    goodreadsData.tags = await downloadListopiaTags(id);

    res.json(goodreadsData);
}

async function createBook(req, res) {
    const bookData = req.body;
    const book = removeEmptyFields(bookData);
    book.createdDate = new Date();

    const result = await db.getCollection().insertOne(book);
    const insertedBook = result.ops[0];
    res.send(insertedBook);
}

async function getBookById(req, res) {
    const query = queryById(req.params.id);

    const book = await db.getCollection().findOne(query);

    if (book) {
        res.json(book);
    } else {
        res.status(404).json({ error: 'Book not found' });
    }
}

async function updateBookById(req, res) {
    const { id } = req.params;
    const query = queryById(id);
    const bookUpdate = updateQuery(req.body);

    await db.getCollection().updateOne(query, bookUpdate);
    const modifiedBook = await db.getCollection().findOne(query);
    res.json(modifiedBook);
}

async function deleteBookById(req, res) {
    const { id } = req.params;
    const query = queryById(id);

    const { covers = [] } = await db.getCollection().findOne(query);

    await db.getCollection().deleteOne(query);
    try {
        await deleteAllCovers(covers)
    } catch (error) {
        debug(error)
    }
    res.send(`Book ${id} was successfully deleted!`);
}

module.exports = {
    getBooks,
    getSuggestions,
    validateAsinIsNotUsed,
    validateGoodreadsIdIsNotUsed,
    importAmazonBook,
    importGoodreadsBook,
    createBook,
    getBookById,
    updateBookById,
    deleteBookById
};
