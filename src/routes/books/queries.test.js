const { expect } = require('chai');
const { ObjectID } = require('mongodb');
const {
    filteringQuery,
    suggestionQuery,
    sortParameter,
    isUsedByOtherBookQuery,
    updateQuery
} = require('./queries');

describe('books queries', () => {
    describe('filteringQuery()', () => {
        it('returns an empty query if nothing is specified', () => {
            const query = filteringQuery();
            expect(query).to.deep.equal({});
        });

        it('returns a query with the specified genres', () => {
            const mockGenres = 'Genre1,Genre2';
            const expectedQuery = {
                genres: {
                    $all: [
                        'Genre1',
                        'Genre2'
                    ]
                }
            };
            const query = filteringQuery({ genres: mockGenres });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified tags', () => {
            const mockTags = 'Tag1,Tag2';
            const expectedQuery = {
                tags: {
                    $all: [
                        'Tag1',
                        'Tag2'
                    ]
                }
            };
            const query = filteringQuery({ tags: mockTags });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified excludedTags', () => {
            const mockExcludedTags = 'Tag3,Tag4';
            const expectedQuery = {
                tags: {
                    $nin: [
                        'Tag3',
                        'Tag4'
                    ]
                }
            };
            const query = filteringQuery({ excludedTags: mockExcludedTags });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified tags and excludedTags', () => {
            const mockTags = 'Tag1,Tag2';
            const mockExcludedTags = 'Tag3,Tag4';
            const expectedQuery = {
                tags: {
                    $all: ['Tag1', 'Tag2'],
                    $nin: ['Tag3', 'Tag4']
                }
            };
            const query = filteringQuery({
                tags: mockTags,
                excludedTags: mockExcludedTags
            });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified rating', () => {
            const mockRating = '2,3';
            const expectedQuery = {
                rating: {
                    $gte: 2,
                    $lte: 3
                }
            };
            const query = filteringQuery({ rating: mockRating });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified amazon rating', () => {
            const mockAmazonRating = '2,3';
            const expectedQuery = {
                'amazonRating.rating': {
                    $gte: 2,
                    $lte: 3
                }
            };
            const query = filteringQuery({ amazonRating: mockAmazonRating });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified goodreads rating', () => {
            const mockGoodreadsRating = '2,3';
            const expectedQuery = {
                'goodreadsRating.rating': {
                    $gte: 2,
                    $lte: 3
                }
            };
            const query = filteringQuery({ goodreadsRating: mockGoodreadsRating });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified interest range', () => {
            const mockInterestRange = '2,3';
            const expectedQuery = {
                interestLevel: {
                    $gte: 2,
                    $lte: 3
                }
            };
            const query = filteringQuery({ interestRange: mockInterestRange });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified interest range when startValue is zero', () => {
            const mockInterestRange = '0,3';
            const expectedQuery = {
                $or: [
                    { interestLevel: { $exists: false } },
                    { interestLevel: { $gte: 0, $lte: 3 } }
                ]
            };
            const query = filteringQuery({ interestRange: mockInterestRange });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified year range', () => {
            const mockYearRange = '1966,2015';
            const expectedQuery = {
                publicationYear: {
                    $gte: 1966,
                    $lte: 2015
                }
            };
            const query = filteringQuery({ year: mockYearRange });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified page range', () => {
            const mockPageRange = '120,340';
            const expectedQuery = {
                pages: {
                    $gte: 120,
                    $lte: 340
                }
            };
            const query = filteringQuery({ pages: mockPageRange });
            expect(query).to.deep.equal(expectedQuery);
        });


        it('returns a query with read status set as read', () => {
            const mockReadStatus = 'read';
            const expectedQuery = {
                'numberOfTimesRead': {
                    "$gt": 0
                }
            };
            const query = filteringQuery({ readStatus: mockReadStatus });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with read status set as notRead', () => {
            const mockReadStatus = 'notRead';
            const expectedQuery = {
                numberOfTimesRead: {
                    $not: { $gt: 0 }
                }
            };
            const query = filteringQuery({ readStatus: mockReadStatus });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified search phrase', () => {
            const mockSearch = 'king';
            const expectedQuery = { $text: { $search: '"king"' } };
            const query = filteringQuery({ search: mockSearch });
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified shelf name', () => {
            const mockShelf = 'Best Horror Novels';
            const expectedQuery = { shelves: "Best Horror Novels" };
            const query = filteringQuery({ shelf: mockShelf });
            expect(query).to.deep.equal(expectedQuery);
        });
    });

    describe('suggestionQuery()', () => {
        it('returns a query with the specified parameters', () => {
            const expectedQuery = [
                { $match: { title: { $regex: 'king', $options: 'i' } } },
                { $group: { _id: '$title' } },
                { $sort: { score: { $meta: 'textScore' } } },
                { $project: { _id: 0, name: '$_id' } }
            ];
            const query = suggestionQuery('title', 'king');
            expect(query).to.deep.equal(expectedQuery);
        });
    });

    describe('sortParameter()', () => {
        it('returns the correct sort parameter based on the values', () => {
            const expectedSortParameter = {
                title: 1
            };
            const query = sortParameter('title', 1);
            expect(query).to.deep.equal(expectedSortParameter);
        });

        it('returns the correct sort parameter when sortField needs to be mapped', () => {
            const expectedSortParameter = {
                'amazonRating.rating': -1
            };
            const query = sortParameter('amazonRating', -1);
            expect(query).to.deep.equal(expectedSortParameter);
        });
    });

    describe('isUsedByOtherBookQuery()', () => {
        it('returns a query with the specified parameters', () => {
            const expectedQuery = {
                goodreadsId: '110'
            };
            const query = isUsedByOtherBookQuery('goodreadsId', '110');
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query with the specified bookId', () => {
            const bookId = '507f1f77bcf86cd799439011';
            const expectedQuery = {
                goodreadsId: '110',
                _id: {
                    $ne: new ObjectID(bookId)
                }
            };
            const query = isUsedByOtherBookQuery('goodreadsId', '110', bookId);
            expect(query).to.deep.equal(expectedQuery);
        });
    });

    describe('updateQuery()', () => {
        it('returns a query that updates the fields and removes all fields that has empty value', () => {
            const bookData = {
                _id: '5dbdd12c6b058f5b5c34d25f',
                asin: '',
                goodreadsId: null,
                title: 'The Shining',
                author: ['Stephen King'],
                tags: [],
                genres: [],
                publicationYear: null,
                pages: null,
                numberOfTimesRead: null,
                amazonRating: { rating: null, numberOfRatings: null },
                goodreadsRating: { rating: null, numberOfRatings: null },
                description: '<p></p>',
                createdDate: '2019-11-02T18:55:40.418Z'
            };

            const expectedQuery = {
                $set: {
                    title: 'The Shining',
                    author: ['Stephen King']
                },
                $unset: {
                    asin: '',
                    goodreadsId: '',
                    tags: '',
                    genres: '',
                    publicationYear: '',
                    pages: '',
                    numberOfTimesRead: '',
                    amazonRating: '',
                    goodreadsRating: '',
                    description: ''
                }
            };
            const query = updateQuery(bookData);
            expect(query).to.deep.equal(expectedQuery);
        });

        it('returns a query that updates the fields when there is no empty value', () => {
            const bookData = {
                _id: '5dbdd12c6b058f5b5c34d25f',
                asin: 'asin',
                goodreadsId: 20,
                title: 'The Shining',
                author: ['Stephen King'],
                tags: ['on kindle'],
                genres: ['Thriller', 'Suspense'],
                publicationYear: 1977,
                pages: 674 ,
                numberOfTimesRead: 1,
                amazonRating: { rating: 4.7, numberOfRatings: 3621 },
                goodreadsRating: { rating: 4.21, numberOfRatings: 10004 },
                description: '<p>Description</p>',
                createdDate: '2019-11-02T18:55:40.418Z'
            };

            const expectedQuery = {
                $set: {
                    asin: 'asin',
                    goodreadsId: 20,
                    title: 'The Shining',
                    author: ['Stephen King'],
                    tags: ['on kindle'],
                    genres: ['Thriller', 'Suspense'],
                    publicationYear: 1977,
                    pages: 674,
                    numberOfTimesRead: 1,
                    amazonRating: {
                        numberOfRatings: 3621,
                        rating: 4.7
                    },
                    goodreadsRating: {
                        numberOfRatings: 10004,
                        rating: 4.21
                    },
                    description: '<p>Description</p>'
                }
            };
            const query = updateQuery(bookData);
            expect(query).to.deep.equal(expectedQuery);
        });
    });
});
