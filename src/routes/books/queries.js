const { ObjectID } = require('mongodb');
const { isNotEmptyValue } = require('./bookData/createBookData');
const set = require('../../utils/set');

const PROJECTION = {
    title: 1,
    author: 1,
    rating: 1,
    covers: 1
};

const SORT_FIELDS = {
    year: 'publicationYear',
    amazonRating: 'amazonRating.rating',
    goodreadsRating: 'goodreadsRating.rating',
    amazonReviews: 'amazonRating.numberOfRatings',
    goodreadsReviews: 'goodreadsRating.numberOfRatings'
};

const READ_STATUS = {
    read: { $gt: 0 },
    notRead: { $not: { $gt: 0 } }
};

const PROTECTED_FIELDS = ['_id', 'createdDate']

const arrayFilter = (name, value, exluded) => ({
    [name]: {
        ...(value ? { $all: value.split(',') } : {}),
        ...(exluded ? { $nin: exluded.split(',') } : {})
    }
});

const ratingFilter = (name, value) => {
    const [ratingStart, ratingEnd] = value.split(',').map(parseFloat);
    const range = {
        [name]: {
            $gte: ratingStart,
            $lte: ratingEnd
        }
    };

    if (ratingStart === 0) {
        return {
            $or: [{ [name]: { $exists: false } }, { ...range }]
        };
    }

    return range;
};

const textSearch = value => ({
    $text: { $search: `"${value}"` }
});

const regexSearch = (fieldName, value) => ({
    $match: { [fieldName]: { $regex: value, $options: 'i' } }
});

const readFilter = value => ({
    numberOfTimesRead: READ_STATUS[value]
});

const isNotUpdateableField = propName => {
    return PROTECTED_FIELDS.includes(propName);
}

const filteringQuery = (filters = {}) => {
    const {
        genres,
        tags,
        excludedTags,
        rating,
        amazonRating,
        goodreadsRating,
        interestRange,
        year,
        pages,
        readStatus,
        search,
        shelf
    } = filters;
    return {
        ...genres ? arrayFilter('genres', genres) : {},
        ...(tags || excludedTags) ? arrayFilter('tags', tags, excludedTags) : {},
        ...rating ? ratingFilter('rating', rating) : {},
        ...amazonRating ? ratingFilter('amazonRating.rating', amazonRating) : {},
        ...goodreadsRating ? ratingFilter('goodreadsRating.rating', goodreadsRating) : {},
        ...interestRange ? ratingFilter('interestLevel', interestRange) : {},
        ...year ? ratingFilter('publicationYear', year) : {},
        ...pages ? ratingFilter('pages', pages) : {},
        ...readStatus ? readFilter(readStatus) : {},
        ...search ? textSearch(search) : {},
        ...shelf ? { shelves: shelf }: {}
    }
};

const suggestionQuery = (fieldName, search) => [
    ...(search ? [regexSearch(fieldName, search)] : []),
    { $group: { _id: `$${fieldName}` } },
    { $sort: { score: { $meta: 'textScore' } } },
    { $project: { _id: 0, name: '$_id' } }
];

const sortParameter = (sort, order) => {
    const sortField = SORT_FIELDS[sort] || sort;
    return {
        [sortField]: Number(order)
    };
};

const isUsedByOtherBookQuery = (fieldName, value, bookId) => {
    const query = { [fieldName]: value };
    if (bookId) {
        query._id = { $ne: new ObjectID(bookId) }
    }

    return query;
}

const queryById = id => ({
    _id: new ObjectID(id)
});

const updateQuery = (book) => {
    const entries = Object.entries(book);
    return entries.reduce((operators, [propName, value]) => {
        if (isNotUpdateableField(propName)) {
            return operators;
        }

        if (isNotEmptyValue(propName, value)) {
            set(operators, `$set.${propName}`, value);
        } else {
            set(operators, `$unset.${propName}`, '');
        }

        return operators;
    }, {});
}

module.exports = {
    filteringQuery,
    suggestionQuery,
    PROJECTION,
    sortParameter,
    isUsedByOtherBookQuery,
    queryById,
    updateQuery
};

