const Joi = require('joi');

const arrayQuerySchema = Joi.string().regex(/^[\w\s,\-&\/>']+$/);
const ratingQuerySchema = Joi.string().regex(/[0-5],[0-5]/);
const publicationYearQuerySchema = Joi.string().regex(/\d{4},\d{4}/);
const pageQuerySchema = Joi.string().regex(/\d+,\d+/);

const bookFilterSchema = Joi.object({
    sort: Joi.string().valid(
        'title',
        'author',
        'year',
        'pages',
        'rating',
        'amazonRating',
        'goodreadsRating',
        'amazonReviews',
        'goodreadsReviews',
        'createdDate',
        'interestLevel'
    ),
    order: Joi.number().valid(-1, 1),
    startIndex: Joi.number().integer(),
    limit: Joi.number().integer(),
    genres: arrayQuerySchema,
    tags: arrayQuerySchema,
    excludedTags: arrayQuerySchema,
    rating: ratingQuerySchema,
    amazonRating: ratingQuerySchema,
    goodreadsRating: ratingQuerySchema,
    interestRange: ratingQuerySchema,
    year: publicationYearQuerySchema,
    pages: pageQuerySchema,
    readStatus: Joi.string().valid('read', 'notRead'),
    search: Joi.string(),
    shelf: Joi.string()
});

const mongoIdSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
const ratingSchema = Joi.number().precision(1).min(0).max(5).allow(null);
const numberOfRatingsSchema = Joi.number().integer().min(0).allow(null);
const coverSchema = Joi.object({
    url: Joi.string(),
    thumbnail: Joi.string()
});
const asinNumberSchema = Joi.string().regex(/^B[\dA-Za-z]{9}|\d{9}(X|\d)$/);
const goodreadsIdSchema = Joi.number().positive().integer();

const bookSchema = Joi.object({
    _id: mongoIdSchema,
    asin: asinNumberSchema.allow(''),
    goodreadsId: goodreadsIdSchema.allow(null),
    title: Joi.string().required(),
    author: Joi.array().items(Joi.string()).min(1).unique().required(),
    seriesTitle: Joi.string().allow(''),
    seriesIndex: Joi.string().allow(''),
    publicationYear: Joi.number().integer().min(1800).max(2050).allow(null),
    pages: Joi.number().positive().integer().allow(null),
    description: Joi.string(),
    genres: Joi.array().items(Joi.string()).unique(),
    tags: Joi.array().items(Joi.string()).unique(),
    rating: ratingSchema,
    amazonRating: Joi.object({
        rating: ratingSchema,
        numberOfRatings: numberOfRatingsSchema
    }),
    goodreadsRating: Joi.object({
        rating: ratingSchema,
        numberOfRatings: numberOfRatingsSchema
    }),
    interestLevel: Joi.number().integer().max(5).allow(null),
    numberOfTimesRead: Joi.number().integer().min(0).allow(null),
    covers: Joi.array().items(coverSchema).unique(),
    shelves: Joi.array().items(Joi.string()).unique(),
    createdDate: Joi.date(),
    review: Joi.string()
});

const updatedBookSchema = bookSchema.append({
    title: Joi.string(),
    author: Joi.array().items(Joi.string()).min(1).unique()
});

const amazonIdParamSchema = Joi.object({
    id: asinNumberSchema
});

const goodreadsIdParamSchema = Joi.object({
    id: goodreadsIdSchema
});

module.exports = {
    bookFilterSchema,
    bookSchema,
    updatedBookSchema,
    amazonIdParamSchema,
    goodreadsIdParamSchema
};
