const { expect } = require('chai');
const { isNotEmptyValue, removeEmptyFields } = require('./createBookData');

describe('createBookData', () => {
    describe('isNotEmptyValue()', () => {
        describe('returns false', () => {
            it('when value is null', () => {
                const propName = 'pages';
                const value = null;
                expect(isNotEmptyValue(propName, value)).to.be.false;
            });

            it('when value is empty string', () => {
                const propName = 'asin';
                const value = '';
                expect(isNotEmptyValue(propName, value)).to.be.false;
            });

            it('when value is empty array', () => {
                const propName = 'tags';
                const value = [];
                expect(isNotEmptyValue(propName, value)).to.be.false;
            });

            it('when value is object that has only falsy properties', () => {
                const propName = 'goodreadsRating';
                const value = { rating: null, numberOfRatings: null };
                expect(isNotEmptyValue(propName, value)).to.be.false;
            });

            it('when value is empty html paragraph', () => {
                const propName = 'description';
                const value = '<p>&nbsp;</p>';
                expect(isNotEmptyValue(propName, value)).to.be.false;
            });
        });

        describe('returns true', () => {
            it('when value is 0', () => {
                const propName = 'pages';
                const value = 0;
                expect(isNotEmptyValue(propName, value)).to.be.true;
            });

            it('when value is a number', () => {
                const propName = 'pages';
                const value = 12;
                expect(isNotEmptyValue(propName, value)).to.be.true;
            });

            it('when value is a not empty string', () => {
                const propName = 'asin';
                const value = 'asin';
                expect(isNotEmptyValue(propName, value)).to.be.true;
            });

            it('when value is a not empty array', () => {
                const propName = 'tags';
                const value = ['free-book', 'on kindle'];
                expect(isNotEmptyValue(propName, value)).to.be.true;
            });

            it('when value is an object that has at least one truthy property', () => {
                const propName = 'goodreadsRating';
                const value = { rating: 1.2, numberOfRatings: null };
                expect(isNotEmptyValue(propName, value)).to.be.true;
            });

            it('when value is an html paragraph with content', () => {
                const propName = 'description';
                const value = '<p>Something</p>';
                expect(isNotEmptyValue(propName, value)).to.be.true;
            });
        });
    });

    describe('removeEmptyFields()', () => {
        it('removes all fields that has empty value', () => {
            const bookData = {
                _id: '5dbdd12c6b058f5b5c34d25f',
                asin: '',
                goodreadsId: null,
                title: 'The Shining',
                author: ['Stephen King'],
                tags: [],
                genres: [],
                publicationYear: null,
                pages: null,
                numberOfTimesRead: null,
                amazonRating: { rating: null, numberOfRatings: null },
                goodreadsRating: { rating: null, numberOfRatings: null },
                description: '<p></p>',
                createdDate: '2019-11-02T18:55:40.418Z'
            };

            const expectedData = {
                _id: '5dbdd12c6b058f5b5c34d25f',
                title: 'The Shining',
                author: ['Stephen King'],
                createdDate: '2019-11-02T18:55:40.418Z'
            }
            expect(removeEmptyFields(bookData)).to.deep.equal(expectedData);
        });

        it('does not remove fields that have not empty value', () => {
            const bookData = {
                _id: '5dbdd12c6b058f5b5c34d25f',
                asin: 'asin',
                goodreadsId: 20,
                title: 'The Shining',
                author: ['Stephen King'],
                tags: ['on kindle'],
                genres: ['Thriller', 'Suspense'],
                publicationYear: 1977,
                pages: 674 ,
                numberOfTimesRead: 1,
                amazonRating: { rating: 4.7, numberOfRatings: 3621 },
                goodreadsRating: { rating: 4.21, numberOfRatings: 10004 },
                description: '<p>Description</p>',
                createdDate: '2019-11-02T18:55:40.418Z'
            };

            expect(removeEmptyFields(bookData)).to.deep.equal(bookData);
        });
    });
});
