const isObject = require('../../../utils/isObject');

function isNotEmptyHtml(value) {
    const emptyHtmlPattern = /^<p>(\s*|(\&nbsp\;)*)<\/p>(\r\n|\r|\n|↵)?$/g;
    return !emptyHtmlPattern.test(value);
}

function isNotEmptyValue(propName, value) {
    if (propName === 'description') {
        return isNotEmptyHtml(value);
    }

    if (isObject(value)) {
        const entries = Object.entries(value);
        return entries.some(([key, value]) => isNotEmptyValue(key, value));
    }

    if (Array.isArray(value)) {
        return !!value.length;
    }

    return value !== null && value !== '';
}

function removeEmptyFields(data) {
    const entries = Object.entries(data);
    return entries.reduce((resultData, [propName, value]) => {
        if (isNotEmptyValue(propName, value)) {
            resultData[propName] = value;
        }
        return resultData;
    }, {});
}

module.exports = {
    isNotEmptyValue,
    removeEmptyFields
};
