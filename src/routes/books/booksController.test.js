const { expect } = require('chai');
const { ObjectID } = require('mongodb');
const nock = require('nock');
const sinon = require('sinon');
const fs = require('fs');
const db = require('../../db/db');
const config = require('../../../config/config');
const app = require('../../app');
const request = require('supertest')(app);
const { copyFile, getFullFilePath } = require('../../utils/fileUtils');
const getHtml = require('../../../test/getHtml');

describe('booksController', () => {
    beforeEach(async () => {
        await db.connect(config.dbName.test);
    });

    afterEach(async () => {
        await db.getCollection().deleteMany({});
        await db.close();
    });

    describe('GET /api/books', () => {
        const mockBooks = [
            {
                title: 'title1',
                genres: ['Action', 'Horror', 'Romance']
            },
            {
                title: 'title2',
                genres: ['Horror', 'Sci-fi', 'Romance']
            },
            {
                title: 'title3',
                genres: ['Adventure', 'Drama', 'Romance']
            }
        ];

        beforeEach(async () => {
            await db.getCollection().insertMany(mockBooks);
        });

        it('should return all books', async () => {
            const { body } = await request.get('/api/books')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.books.length).to.equal(mockBooks.length);
            expect(body.books[0]).to.have.property('title').and.equal(mockBooks[0].title);
        });

        it('should return all books with the specified order', async () => {
            const { body } = await request.get('/api/books?order=-1')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.books.length).to.equal(mockBooks.length);
            expect(body.books[0]).to.have.property('title').and.equal(mockBooks[mockBooks.length - 1].title);
        });

        it('should return filtered books if one is specified in the query', async () => {
            const { body } = await request.get('/api/books?genres=Sci-fi')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.books.length).to.equal(1);
            expect(body.books[0]).to.have.property('title').and.equal(mockBooks[1].title);
        });

        it('should return filtered books if multiple are specified in the query', async () => {
            const { body } = await request.get('/api/books?genres=Horror,Romance')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.books.length).to.equal(2);
            expect(body.books[0]).to.have.property('title').and.equal(mockBooks[0].title);
            expect(body.books[1]).to.have.property('title').and.equal(mockBooks[1].title);
        });

        it('should return validation error if separator is invalid in the query', async () => {
            const { body } = await request.get('/api/books?genres=Horror;Romance')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('genres');
        });

        it('should return additional paging information', async () => {
            const { body } = await request.get('/api/books')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.have.property('count').and.equal(mockBooks.length);
            expect(body).to.have.property('hasNext').and.to.be.false; // eslint-disable-line no-unused-expressions
        });

        it('should return validation error if query is invalid', async () => {
            const invalidSortOrder = 3;
            const { body } = await request.get(`/api/books?order=${invalidSortOrder}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('order');
        });

        it('should respond with database error', async () => {
            const expectedError = new Error('Database error');
            const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
            const { body } = await request.get('/api/books')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('stack').and.to.includes('Database error');
            getCollectionStub.restore();
        });
    });

    describe('GET /api/books/suggestions', () => {
        const mockBooks = [
            {
                title: 'The Mirror King',
                author: ['Jodi Meadows'],
                seriesTitle: 'The Orphan Queen',
                genres: ['Fantasy', 'Young Adult', 'Romance']
            },
            {
                title: 'The Gunslinger',
                author: ['Stephen King'],
                seriesTitle: 'The Dark Tower',
                genres: ['Fantasy', 'Horror', 'Sci-fi', 'Fiction']
            },
            {
                title: 'The Wise Man\'s Fear',
                author: ['Patrick Rothfuss'],
                seriesTitle: 'The Kingkiller Chronicle',
                genres: ['Fantasy', 'Fiction', 'Epic Fantasy']
            }
        ];

        beforeEach(async () => {
            await db.getCollection().insertMany(mockBooks);
            await db.getCollection().createIndex(
                {
                  "title":"text",
                  "author":"text",
                  "seriesTitle":"text"
                },
                {
                  name: "searchIndex"
                }
             )
        });

        afterEach(async () => {
            await db.getCollection().dropIndex("searchIndex")
        });

        it('should return all suggestions when query is not specified', async () => {
            const expectedSuggestions = {
                titles: [
                    { name: 'The Wise Man\'s Fear' },
                    { name: 'The Gunslinger' },
                    { name: 'The Mirror King' }
                ],
                authors: [
                    { name: 'Patrick Rothfuss' },
                    { name: 'Stephen King' },
                    { name: 'Jodi Meadows' }
                ],
                seriesTitles: [
                    { name: 'The Kingkiller Chronicle' },
                    { name: 'The Dark Tower' },
                    { name: 'The Orphan Queen' }
                ]
            };
            const { body } = await request.get('/api/books/suggestions')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSuggestions);
        });

        it('should return suggestions based on the specified genre', async () => {
            const expectedSuggestions = {
                titles: [{ name: 'The Gunslinger' }],
                authors: [{ name: 'Stephen King' }],
                seriesTitles: [{ name: 'The Dark Tower' }]
            };
            const { body } = await request.get('/api/books/suggestions?genres=Horror')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSuggestions);
        });

        it('should return suggestions based on multiple genre', async () => {
            const expectedSuggestions = {
                titles: [
                    { name: 'The Wise Man\'s Fear' },
                    { name: 'The Gunslinger' }
                ],
                authors: [
                    { name: 'Patrick Rothfuss' },
                    { name: 'Stephen King' }
                ],
                seriesTitles: [
                    { name: 'The Kingkiller Chronicle' },
                    { name: 'The Dark Tower' }
                ]
            };
            const { body } = await request.get('/api/books/suggestions?genres=Fantasy,Fiction')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSuggestions);
        });

        it('should return suggestions based on multiple criteria', async () => {
            const expectedSuggestions = {
                titles: [],
                authors: [{ name: 'Stephen King' }],
                seriesTitles: []
            };
            const { body } = await request.get('/api/books/suggestions?genres=Fantasy,Fiction&search=king')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSuggestions);
        });

        it('should return validation error if separator is invalid in the query', async () => {
            const { body } = await request.get('/api/books/suggestions?genres=Horror;Romance')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('genres');
        });

        it('should respond with database error', async () => {
            const expectedError = new Error('Database error');
            const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
            const { body } = await request.get('/api/books/suggestions')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('stack').and.to.includes('Database error');
            getCollectionStub.restore();
        });
    });

    describe('GET /api/books/import/amazon/{id}', () => {
        it('should return amazonData based on the given id', async () => {
            const expectedBook = {
                asin: 'B00M44051E',
                title: 'The Invisible Library',
                author: ['Genevieve Cogman'],
                seriesTitle: 'The Invisible Library Novel',
                seriesIndex: '1',
                pages: 337,
                publicationYear: 2014,
                description:
                    '<div><p><b>The first in a gripping, adventurous series,<i> The Invisible Library </i>is the astounding debut from Genevieve Cogman.</b><br /><br /><b>Irene must be at the top of her game or she&apos;ll be off the case &#x2013; permanently . . .</b><br /><br />Irene is a professional spy for the mysterious Library, which harvests fiction from different realities. And along with her enigmatic assistant Kai, she&apos;s posted to an alternative London. Their mission &#x2013; to retrieve a dangerous book. But when they arrive, it&apos;s already been stolen. London&apos;s underground factions seem prepared to fight to the very death to find <i>her</i> book.<br /><br />Adding to the jeopardy, this world is chaos-infested &#x2013; the laws of nature bent to allow supernatural creatures and unpredictable magic. Irene&apos;s new assistant is also hiding secrets of his own.<br /><br />Soon, she&apos;s up to her eyebrows in a heady mix of danger, clues and secret societies. Yet failure is not an option &#x2013; the nature of reality itself is at stake.<br /><br /><i>The Invisible Library </i>is followed by the second title in The Invisible Library series, <i>The Masked City.</i></p></div>\n<em></em>',
                genres: [
                    'Alternative History',
                    'Contemporary Fantasy Fiction',
                    'Alternate History Science Fiction',
                ],
                amazonRating: { rating: 4.2, numberOfRatings: 2024 }
            };

            const html = await getHtml('a_Genevieve Cogman - The Invisible Library');
            nock('https://www.amazon.com')
                .get('/dp/B00M44051E')
                .reply(200, html);
            const { body } = await request.get('/api/books/import/amazon/B00M44051E')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedBook);
        });
    });

    describe('GET /api/books/validate/asin/{id}', () => {
        const mockBooks = [
            {
                _id: new ObjectID('507f1f77bcf86cd799439011'),
                asin: 'B004XISI4A',
                goodreadsId: 11325938
            },
            {
                _id: new ObjectID('507f1f77bcf86cd799439012'),
                asin: 'B001L10YA8',
                goodreadsId: 6569833
            },
            {
                _id: new ObjectID('507f1f77bcf86cd799439013'),
                asin: 'B004P1JEWU',
                goodreadsId: 11133581
            }
        ];

        beforeEach(async () => {
            await db.getCollection().insertMany(mockBooks);
        });

        it('should return true when asin number is not in the database', async () => {
            const { body } = await request.get('/api/books/validate/asin/B001111111')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.be.true;
        });

        it('should return true when asin number is only used by that book', async () => {
            const bookId = '507f1f77bcf86cd799439012';
            const { body } = await request.get(`/api/books/validate/asin/B001L10YA8?bookId=${bookId}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.be.true;
        });

        it('should return false when asin number is already in the database', async () => {
            const { body } = await request.get('/api/books/validate/asin/B001L10YA8')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.be.false;
        });
    });

    describe('GET /api/books/validate/goodreadsId/{id}', () => {
        const mockBooks = [
            {
                _id: new ObjectID('507f1f77bcf86cd799439011'),
                asin: 'B004XISI4A',
                goodreadsId: 11325938
            },
            {
                _id: new ObjectID('507f1f77bcf86cd799439012'),
                asin: 'B001L10YA8',
                goodreadsId: 6569833
            },
            {
                _id: new ObjectID('507f1f77bcf86cd799439013'),
                asin: 'B004P1JEWU',
                goodreadsId: 11133581
            }
        ];

        beforeEach(async () => {
            await db.getCollection().insertMany(mockBooks);
        });

        it('should return true when goodreadsId is not in the database', async () => {
            const { body } = await request.get('/api/books/validate/goodreadsId/11')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.be.true;
        });

        it('should return true when goodreadsId is only used by that book', async () => {
            const bookId = '507f1f77bcf86cd799439011';
            const { body } = await request.get(`/api/books/validate/goodreadsId/11325938?bookId=${bookId}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.be.true;
        });

        it('should return false when goodreadsId is already in the database', async () => {
            const { body } = await request.get('/api/books/validate/goodreadsId/11325938')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.be.false;
        });
    });

    describe('GET /api/books/import/goodreads/{id}', () => {
        it('should return goodreadsData based on the given id', async () => {
            await db.getCollection('normalizations').insertOne({
                name: 'tags'
            });

            const expectedBook = {
                goodreadsId: 1,
                title: 'Morning Star',
                author: ['Pierce Brown'],
                seriesTitle: 'Red Rising Saga',
                seriesIndex: '3',
                publicationYear: 2016,
                pages: 524,
                description:
                    '#1 NEW YORK TIMES BESTSELLER • Red Rising thrilled readers and announced the presence of a talented new author. Golden Son changed the game and took the story of Darrow to the next level. Now comes the exhilarating conclusion to the Red Rising Trilogy: Morning Star.<br><br>Darrow would have lived in peace, but his enemies brought him war. The Gold overlords demanded his obedience, hanged his wife, and enslaved his people. But Darrow is determined to fight back. Risking everything to transform himself and breach Gold society, Darrow has battled to survive the cutthroat rivalries that breed Society’s mightiest warriors, climbed the ranks, and waited patiently to unleash the revolution that will tear the hierarchy apart from within.<br><br>Finally, the time has come.<br><br>But devotion to honor and hunger for vengeance run deep on both sides. Darrow and his comrades-in-arms face powerful enemies without scruple or mercy. Among them are some Darrow once considered friends. To win, Darrow will need to inspire those shackled in darkness to break their chains, unmake the world their cruel masters have built, and claim a destiny too long denied—and too glorious to surrender.',
                genres: [
                    'Science Fiction',
                    'Fantasy',
                    'Fiction',
                    'Science Fiction > Dystopia',
                    'Young Adult'
                ],
                originalGenres: [
                    'Science Fiction',
                    'Fantasy',
                    'Fiction',
                    'Science Fiction > Dystopia',
                    'Young Adult'
                ],
                tags: [
                    'to-read',
                    'currently-reading',
                    'sci-fi',
                    'favorites',
                    'science-fiction',
                    'fantasy',
                    'fiction',
                    'dystopian',
                    'dystopia',
                    'young-adult',
                    'owned',
                    'scifi',
                    'series',
                    'audiobook',
                    'own',
                    'ya',
                    'books-i-own',
                    'audiobooks',
                    'favourites',
                    'audible',
                    'adult',
                    'sci-fi-fantasy',
                    'to-buy',
                    'wishlist',
                    'audio',
                    'red-rising',
                    'space-opera',
                    'ebook',
                    'space',
                    'adventure',
                    'war',
                    'library',
                    'fantasy-sci-fi',
                    'owned-books',
                    'sf',
                    'tbr',
                    'all-time-favorites',
                    'action',
                    'scifi-fantasy',
                    'audio-books',
                    'romance',
                    'ebooks',
                    'owned-tbr',
                    'adult-fiction',
                    'favorite',
                    'my-books',
                    'dnf',
                    'favorite-books',
                    'physical-tbr',
                    'pierce-brown',
                    'wish-list',
                    'novels',
                    'science-fiction-fantasy',
                    'audio-book',
                    'my-library',
                    'thriller',
                    'military',
                    'epic',
                    'futuristic',
                    'e-book',
                    'speculative-fiction',
                    're-read',
                    'favorite-series',
                    'need-to-buy',
                    'reread',
                    'new-adult',
                    'i-own',
                    'action-adventure',
                    'signed',
                    'dark',
                    'did-not-finish',
                    'red-rising-saga',
                    'made-me-cry',
                    'series-to-finish'
                ],
                goodreadsRating: {
                    rating: 4.49,
                    numberOfRatings: 95846
                }
            };

            const html = await getHtml('g_Pierce Brown - Morning Star');
            const shelvesHtml = await getHtml('g_Pierce Brown - Morning Star - Top Shelves');
            nock('https://www.goodreads.com')
                .get('/book/show/1')
                .reply(200, html);
            nock('https://www.goodreads.com')
                .get('/book/shelves/1')
                .reply(200, shelvesHtml);
            const { body } = await request.get('/api/books/import/goodreads/1')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedBook);

            await db.getCollection('normalizations').deleteMany({});
        });
    });

    describe('POST /api/books', () => {
        it('should successfully create a new book', async () => {
            const mockBook = {
                title: 'title',
                author: ['Jane Austen']
            };
            const { body } = await request.post('/api/books')
                .type('json')
                .send(mockBook)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.have.property('_id');
            expect(body).to.have.property('createdDate');
            expect(body).to.have.property('title').and.equal(mockBook.title);
        });

        it('should not create a book with invalid title', async () => {
            const mockBook = {
                title: 1,
                author: ['Stephen King']
            };
            const { body } = await request.post('/api/books')
                .type('json')
                .send(mockBook)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('title');
        });

        it('should not create a book without title', async () => {
            const mockBook = {
                author: ['Stephen King']
            };
            const { body } = await request.post('/api/books')
                .type('json')
                .send(mockBook)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('title');
        });
    });

    describe('Based on given id', () => {
        const expectedId = '507f1f77bcf86cd799439011';
        const coverPath = '/images/covers/testImage.jpg';
        const smallCoverPath = '/images/smallcovers/testImage.jpg';
        const mockBook = {
            _id: new ObjectID(expectedId),
            title: 'title',
            pages: 200,
            covers: [
                {
                    url: coverPath,
                    thumbnail: smallCoverPath
                }
            ]
        };

        beforeEach(async () => {
            await db.getCollection().insertOne(mockBook);
        });

        describe('GET /api/books/{id}', () => {
            it('should return a book by the given id', async () => {
                const { body } = await request.get(`/api/books/${expectedId}`)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200);
                expect(body).to.have.property('_id').and.equal(expectedId);
                expect(body).to.have.property('title').and.equal(mockBook.title);
            });

            it('should return 404 with invalid id', async () => {
                await request.get('/api/books/1')
                    .set('Accept', 'application/json')
                    .expect(404);
            });

            it('should respond with 404 when the book is not found', async () => {
                const mockId = '5cc53dc17ffdf4c78deab30c';
                const { body } = await request.get(`/api/books/${mockId}`)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(404);
                expect(body).to.have.property('error').and.equal('Book not found');
            });

            it('should respond with database error', async () => {
                const expectedError = new Error('Database error');
                const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
                const { body } = await request.get(`/api/books/${expectedId}`)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(500);
                expect(body).to.have.property('stack').and.to.includes('Database error');
                getCollectionStub.restore();
            });
        });

        describe('PUT /api/books/{id}', () => {
            it('should update the book by the given id', async () => {
                const updatedBook = {
                    title: 'Updated Title',
                    author: ['Stephen King']
                };
                const { body } = await request.put(`/api/books/${expectedId}`)
                    .type('json')
                    .send(updatedBook)
                    .set('Accept', 'application/json')
                    .expect(200);
                expect(body).to.have.property('title').and.equal(updatedBook.title);
                expect(body).to.have.property('author').and.deep.equal(updatedBook.author);
            });

            it('should only modify the updated values', async () => {
                const updatedBook = {
                    author: ['Stephen King']
                };
                const { body } = await request.put(`/api/books/${expectedId}`)
                    .type('json')
                    .send(updatedBook)
                    .set('Accept', 'application/json')
                    .expect(200);
                expect(body).to.have.property('title').and.equal(mockBook.title);
                expect(body).to.have.property('author').and.deep.equal(updatedBook.author);
            });

            it('should remove empty field during update', async () => {
                const updatedBook = {
                    title: 'Updated Title',
                    author: ['Stephen King'],
                    pages: null
                };
                const { body } = await request.put(`/api/books/${expectedId}`)
                    .type('json')
                    .send(updatedBook)
                    .set('Accept', 'application/json')
                    .expect(200);
                expect(body).to.have.property('title').and.equal(updatedBook.title);
                expect(body).to.have.property('author').and.deep.equal(updatedBook.author);
                expect(body).to.not.have.property('pages');
            });

            it('should not update a book with invalid title', async () => {
                const updatedBook = {
                    title: 1
                };
                const { body } = await request.put(`/api/books/${expectedId}`)
                    .type('json')
                    .send(updatedBook)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(422);
                expect(body).to.have.property('title').and.equal('ValidationError');
                expect(body).to.have.property('source').and.equal('title');
            });


            it('should return 404 with invalid id', async () => {
                await request.put('/api/books/1')
                    .set('Accept', 'application/json')
                    .expect(404);
            });

            it('should respond with database error', async () => {
                const expectedError = new Error('Database error');
                const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
                const { body } = await request.put(`/api/books/${expectedId}`)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(500);
                expect(body).to.have.property('stack').and.to.includes('Database error');
                getCollectionStub.restore();
            });
        });

        describe('DELETE /api/books/{id}', () => {
            it('should delete the book by the given id', async () => {
                const expectedResponse = `Book ${expectedId} was successfully deleted!`;
                const { text } = await request.delete(`/api/books/${expectedId}`)
                    .set('Accept', 'application/json')
                    .expect(200);
                expect(text).to.equal(expectedResponse);
            });

            it('should delete covers of the deleted book', async () => {
                const testImage = './test/testImage.jpg';
                await copyFile(testImage, getFullFilePath(coverPath));
                await copyFile(testImage, getFullFilePath(smallCoverPath));
                const deleteSpy = sinon.spy(fs, 'unlink');

                await request.delete(`/api/books/${expectedId}`)
                    .set('Accept', 'application/json')
                    .expect(200);
                expect(deleteSpy.getCall(0).args[0]).to.equal(getFullFilePath(coverPath));
                expect(deleteSpy.getCall(1).args[0]).to.equal(getFullFilePath(smallCoverPath));
                deleteSpy.restore();
            });

            it('should not throw when the book does not have covers', async () => {
                const bookIdWithoutCovers = '507f1f77bcf86cd799439012';
                const mockBookWithoutCover = {
                    _id: new ObjectID(bookIdWithoutCovers),
                    title: 'title2',
                    pages: 400
                };
                await db.getCollection().insertOne(mockBookWithoutCover);

                await request.delete(`/api/books/${bookIdWithoutCovers}`)
                    .set('Accept', 'application/json')
                    .expect(200);
                await db.getCollection().deleteOne({ _id: new ObjectID(bookIdWithoutCovers) });
            });

            it('should return 404 with invalid id', async () => {
                await request.delete('/api/books/1')
                    .set('Accept', 'application/json')
                    .expect(404);
            });

            it('should respond with database error', async () => {
                const expectedError = new Error('Database error');
                const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
                const { body } = await request.delete(`/api/books/${expectedId}`)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(500);
                expect(body).to.have.property('stack').and.to.includes('Database error');
                getCollectionStub.restore();
            });
        });
    });
});
