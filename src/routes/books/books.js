const express = require('express');
const {
    getBooks,
    getSuggestions,
    validateAsinIsNotUsed,
    validateGoodreadsIdIsNotUsed,
    importAmazonBook,
    importGoodreadsBook,
    createBook,
    getBookById,
    updateBookById,
    deleteBookById
} = require('./booksController');
const {
    bookFilterSchema,
    bookSchema,
    updatedBookSchema,
    amazonIdParamSchema,
    goodreadsIdParamSchema
} = require('./booksModel');
const asyncErrorHandler = require('../../middlewares/asyncErrorHandler');
const { validateQuery, validateParams, validateBody } = require('../../middlewares/validation');

const router = express.Router();

router.get('/', validateQuery(bookFilterSchema), asyncErrorHandler(getBooks));
router.get('/suggestions', validateQuery(bookFilterSchema), asyncErrorHandler(getSuggestions));
router.get('/validate/asin/:id', validateParams(amazonIdParamSchema), asyncErrorHandler(validateAsinIsNotUsed));
router.get('/validate/goodreadsId/:id', validateParams(goodreadsIdParamSchema), asyncErrorHandler(validateGoodreadsIdIsNotUsed));
router.get('/import/amazon/:id', validateParams(amazonIdParamSchema), asyncErrorHandler(importAmazonBook));
router.get('/import/goodreads/:id', validateParams(goodreadsIdParamSchema), asyncErrorHandler(importGoodreadsBook));
router.post('/', validateBody(bookSchema), asyncErrorHandler(createBook));
router.get('/:id([a-fA-F0-9]{24})', asyncErrorHandler(getBookById));
router.put('/:id([a-fA-F0-9]{24})', validateBody(updatedBookSchema), asyncErrorHandler(updateBookById));
router.delete('/:id([a-fA-F0-9]{24})', asyncErrorHandler(deleteBookById));

module.exports = router;
