const cheerio = require('cheerio');
const normalizeGenres = require('./normalizeGenres');
const stringToNumber = require('./stringToNumber');

const SELECTORS = {
    title: '#bookTitle',
    author: '.authorName',
    seriesDataFromTitle: '#bookSeries > a',
    seriesDataFromInfo: '#bookDataBox > .clearFloats div:contains("Series") ~ .infoBoxRowItem',
    firstPublished: '#details > .row > nobr',
    otherPublicationDate: '#details > .row',
    pages: 'span[itemprop="numberOfPages"]',
    descriptionLong: '#description > span:nth-child(2)',
    descriptionShort: '#description > span:nth-child(1)',
    genres: '.bigBoxContent > .elementList > .left',
    rating: '#bookMeta span[itemprop="ratingValue"]',
    numberOfRatings: '#bookMeta meta[itemprop="ratingCount"]'
};

async function getGoodreadsData(html) {
    const $ = cheerio.load(html);

    const title = getTitle($);
    const { seriesTitle = null, seriesIndex = null } = getSeriesData($);
    const genres = getGenres($);
    const normalizedGenres = await normalizeGenres(genres);

    const goodreadsData = {
        title: title,
        author: getAuthor($),
        seriesTitle: seriesTitle,
        seriesIndex: seriesIndex,
        publicationYear: getPublicationYear($),
        pages: getPages($),
        description: getDescription($),
        genres: normalizedGenres,
        originalGenres: genres,
        goodreadsRating: getRatingData($)
    };

    return goodreadsData;
}

function getTitle($) {
    return extractTitle() || null;

    function extractTitle() {
        return $(SELECTORS.title)
            .contents()
            .filter((i, elem) => elem.nodeType == 3)
            .text()
            .trim();
    }
}

function getAuthor($) {
    return $(SELECTORS.author)
        .map((i, elem) => {
            const data = $(elem).text();
            return data.replace(/\s+/, ' ');
        })
        .get();
}

function getSeriesData($) {
    const seriesData = $(SELECTORS.seriesDataFromTitle)
        .text()
        .trim()
        .slice(1, -1);

    if (!seriesData) {
        return {};
    }

    return splitData();

    function splitData() {
        const parts = seriesData.split(' #');
        const seriesTitle = parts[0].trim();
        const seriesIndex = parts[1];

        return {
            seriesTitle,
            seriesIndex
        };
    }
}

function getPages($) {
    const pages = $(SELECTORS.pages)
        .text()
        .replace(' pages', '');

    return stringToNumber(pages);
}

function getPublicationYear($) {
    const publicationDate = extractFirstPublicationDate() || extractOtherPublicationDate();
    const matches = /\d{4}/.exec(publicationDate);

    if (!publicationDate || !matches) {
        return null;
    }

    return Number(matches[0]);

    function extractFirstPublicationDate() {
        return $(SELECTORS.firstPublished)
            .text()
            .trim();
    }

    function extractOtherPublicationDate() {
        return $(SELECTORS.otherPublicationDate)
            .text()
            .trim();
    }
}

function getGenres($) {
    return $(SELECTORS.genres)
        .map((i, elem) => {
            const data = $(elem).text();
            return formatNestedGenres(data.trim());
        })
        .get();

    function formatNestedGenres(text) {
        return text.replace(/>\s+/g, '> ');
    }
}

function getDescription($) {
    return (
        $(SELECTORS.descriptionLong).html() ||
        $(SELECTORS.descriptionShort).html()
    );
}

function getRatingData($) {
    const rating = stringToNumber(extractRating());
    const numberOfRatings = stringToNumber(extractNumberOfRatings());

    return {
        rating,
        numberOfRatings
    };

    function extractRating() {
        return $(SELECTORS.rating).text();
    }

    function extractNumberOfRatings() {
        return $(SELECTORS.numberOfRatings).attr('content');
    }
}

module.exports = getGoodreadsData;
