const cheerio = require('cheerio');
const normalizeTitle = require('./normalizeTitle.js');
const stringToNumber = require('./stringToNumber');

const SELECTORS = {
    title: '#productTitle',
    authorsWithArrow: 'span.author:contains("(Author)") .contributorNameID',
    authorsWithoutArrow: 'span.author:contains("(Author)") > a.a-link-normal',
    seriesData: '#reviewFeatureGroup > span',
    pagesEbook: '#rich_product_information .rpi-attribute-label:contains("length") ~ .rpi-attribute-value',
    pagesDetails: '#detailBullets_feature_div li .a-text-bold:contains("Print length") ~ span',
    pagesPaperBack: '#detailBullets_feature_div li .a-text-bold:contains("Paperback") ~ span',
    publicationYear: '#detailBullets_feature_div li:contains("Publication Date")',
    publicationYearFromPublisher: '#detailBullets_feature_div li:contains("Publisher")',
    description: '#bookDescription_feature_div > noscript',
    genresBreadcrumb: '#wayfinding-breadcrumbs_feature_div .a-link-normal',
    genres: 'ul .zg_hrsr a',
    rating: '#averageCustomerReviews span.reviewCountTextLinkedHistogram',
    numberOfReviews: '#acrCustomerReviewText',
    asin: '#detailBullets_feature_div li:contains("ASIN")'
};

function getAmazonData(html) {
    const $ = cheerio.load(html);

    const {
        title = null,
        seriesIndex = null,
        seriesTitle = null,
    } = getTitleAndSeries($);

    const amazonData = {
        title: title,
        author: getAuthor($),
        seriesTitle: seriesTitle,
        seriesIndex: seriesIndex,
        pages: getPages($),
        publicationYear: getPublicationYear($),
        description: getDescription($),
        genres: getGenres($),
        amazonRating: getRatingData($),
        asin: getAsinNumber($)
    };

    return amazonData;
}

function getTitleAndSeries($) {
    const {
        title: normalizedTitle,
        series: normalizedSeries
    } = getNormalizedTitleData();
    let seriesData = getSeriesData($);

    if (!seriesData && normalizedSeries) {
        seriesData = splitNormalizedSeries();
    }

    return {
        title: normalizedTitle,
        ...seriesData
    };

    function getNormalizedTitleData() {
        const data = $(SELECTORS.title).text();
        const title = data.trim();

        if (!title) {
            return {};
        }

        return normalizeTitle(title);
    }

    function splitNormalizedSeries() {
        const [seriesTitle, seriesIndex] = normalizedSeries.split(' #');
        return {
            seriesTitle,
            seriesIndex
        };
    }
}

function getSeriesData($) {
    const seriesData = $(SELECTORS.seriesData)
        .text()
        .trim();

    if (!seriesData) {
        return null;
    }

    return splitData();

    function splitData() {
        const pattern = /Book (\d+)\s+of (\d+):\s+(.+?)( Series)?(\s+\(\d+ Book Series\))?$/;
        const matches = pattern.exec(seriesData);

        return {
            seriesIndex: matches[1],
            seriesTitle: matches[3]
        };
    }
}

function getAuthor($) {
    const removeSpaceBetweenInitial = /^([A-Z]\.)\s([A-Z].\s.*)/;
    let authors = extractAuthorData(SELECTORS.authorsWithArrow);
    authors.push(...extractAuthorData(SELECTORS.authorsWithoutArrow));

    return authors;

    function extractAuthorData(selector) {
        return $(selector)
            .map((i, elem) => {
                const data = $(elem).text();
                return data.replace(removeSpaceBetweenInitial, '$1$2');
            })
            .get();
    }
}

function getPages($) {
    const pages = extractFromEbook() || extractFromDetails() || extractFromPaperback();

    return stringToNumber(pages);

    function extractFromEbook() {
        return $(SELECTORS.pagesEbook)
            .text()
            .trim()
            .split(' ')[0];
    }

    function extractFromDetails() {
        return $(SELECTORS.pagesDetails)
            .text()
            .trim()
            .split(' ')[2];
    }

    function extractFromPaperback() {
        return $(SELECTORS.pagesPaperBack)
            .text()
            .replace(/[\D]/g, '');
    }
}

function getPublicationYear($) {
    const publicationDate = extractPublicationDate() || extractPublisher();
    const matches = /\d{4}/.exec(publicationDate);

    if (!publicationDate || !matches) {
        return null;
    }

    return Number(matches[0]);

    function extractPublicationDate() {
        return $(SELECTORS.publicationYear)
            .text()
            .trim();
    }

    function extractPublisher() {
        return $(SELECTORS.publicationYearFromPublisher)
            .text()
            .trim();
    }
}

function getGenres($) {
    const breadcrumb = getBreadcrumb($);
    const genres = $(SELECTORS.genres)
            .map((i, elem) => {
                const data = $(elem).text();
                const normalizedData = removeUnnecessaryPart(data);
                return normalizedData.split(' > ');
            })
            .get();
    const uniqueGenres = new Set([].concat(...breadcrumb, ...genres));
    return [...uniqueGenres];

    function getBreadcrumb($) {
        return $(SELECTORS.genresBreadcrumb)
            .map((i, elem) => {
                return $(elem).text().trim();
            })
            .get()
            .slice(1)
            .filter(value => value !== 'Kindle eBooks');
    }

    function removeUnnecessaryPart(text) {
        return text
            .replace(/in\s/, '')
            .replace(/\s\((Books|Kindle Store)\)/, '');
    }
}

function getDescription($) {
    const description = $('#bookDescription_feature_div > noscript')
        .text()
        .trim();

    return description || null;
}

function getRatingData($) {
    const rating = stringToNumber(extractRating());
    const numberOfReviews = stringToNumber(extractNumberOfReviews());

    return {
        rating,
        numberOfRatings: numberOfReviews
    };

    function extractRating() {
        const data = $(SELECTORS.rating).attr('title') || '';

        return data.split(' ')[0];
    }

    function extractNumberOfReviews() {
        return removeComma(
            $(SELECTORS.numberOfReviews)
                .text()
                .split(' ')[0]
        );
    }

    function removeComma(text) {
        return text.replace(',', '');
    }
}

function getAsinNumber($) {
    const asinPattern = /([A-Z0-9]{10})/;
    const matches = $(SELECTORS.asin)
        .text()
        .match(asinPattern)
    const data = matches ? matches[1] : '';
    return data.trim();
}

module.exports = getAmazonData;

/*
Error handling

When selector doesn't match it mostly doesn't cause errors:
    text() => ''
    map().get() => []

Processing empty result:
    ''.split[0] => ''
    ''.split[2] => undefined

BUT:
    attr() => undefined
    > need a fallback value for further processing

*/
