const { expect } = require('chai');
const getAmazonData = require('./getAmazonData.js');
const getHtml = require('../../../../test/getHtml');

describe('getAmazonData()', () => {
    describe('when Kindle edition', () => {
        it('should return the title', async () => {
            const filename = 'a_Maria Luisa Minarelli - Murder in Venice_eBook';
            const expected = 'Murder in Venice';
            const html = await getHtml(filename);

            expect(getAmazonData(html).title).to.equal(expected);
        });
    });

    describe('when Paperback edition', () => {
        it('should return the title', async () => {
            const filename = 'a_Maria Luisa Minarelli - Murder in Venice_paperback';
            const expected = 'Murder in Venice (Venice Mystery)';
            const html = await getHtml(filename);

            expect(getAmazonData(html).title).to.equal(expected);
        });
    });

    describe('when there is one author', () => {
        it('should return one author', async () => {
            const filename = 'a_Genevieve Cogman - The Invisible Library';
            const expected = ['Genevieve Cogman'];
            const html = await getHtml(filename);

            expect(getAmazonData(html).author).to.deep.equal(expected);
        });
    });

    describe('when there are multiple authors', () => {
        describe('when all authors have popover', () => {
            it('should return multiple authors', async () => {
                const filename =
                    'a_John Lewis, Andrew Aydin - March - Book Three';
                const expected = ['John Lewis', 'Andrew Aydin'];
                const html = await getHtml(filename);

                expect(getAmazonData(html).author).to.deep.equal(expected);
            });
        });

        describe('when only some authors have popover', () => {
            it('should return multiple authors', async () => {
                const filename = 'a_Iris Johansen, Roy Johansen - Night Watch';
                const expected = ['Iris Johansen', 'Roy Johansen'];
                const html = await getHtml(filename);

                expect(getAmazonData(html).author).to.deep.equal(expected);
            });
        });

        describe('when neither author has popover', () => {
            it('should return multiple authors', async () => {
                const filename =
                    'a_Jun Eishima, Yoko Taro - NieR - Automata - Short Story Long';
                const expected = ['Jun Eishima', 'Yoko Taro'];
                const html = await getHtml(filename);

                expect(getAmazonData(html).author).to.deep.equal(expected);
            });
        });
    });

    describe('when series data is available', () => {
        describe('when series data does not contain brackets', () => {
            it('should return series data', async () => {
                const filename = 'a_Genevieve Cogman - The Invisible Library';
                const expectedSeries = 'The Invisible Library Novel';
                const expectedSeriesIndex = '1';

                const html = await getHtml(filename);
                const result = getAmazonData(html);

                expect(result.seriesTitle).to.equal(expectedSeries);
                expect(result.seriesIndex).to.equal(expectedSeriesIndex);
            });
        });

        describe('when series data contains brackets', () => {
            it('should return series data', async () => {
                const filename =
                    'a_John Lewis, Andrew Aydin - March - Book Three';
                const expectedSeries = 'March (Issues)';
                const expectedSeriesIndex = '3';

                const html = await getHtml(filename);
                const result = getAmazonData(html);

                expect(result.seriesTitle).to.equal(expectedSeries);
                expect(result.seriesIndex).to.equal(expectedSeriesIndex);
            });
        });

        describe('when series data does not contain "(# Book Series)"', () => {
            it('should return series data', async () => {
                const filename = 'a_Iris Johansen, Roy Johansen - Night Watch';
                const expectedSeries = 'Kendra Michaels';
                const expectedSeriesIndex = '4';

                const html = await getHtml(filename);
                const result = getAmazonData(html);

                expect(result.seriesTitle).to.equal(expectedSeries);
                expect(result.seriesIndex).to.equal(expectedSeriesIndex);
            });
        });
    });

    describe('when series data is only available in the title', () => {
        it('should return series data from the title', async () => {
            const filename = 'a_Maria Luisa Minarelli - Murder in Venice_eBook';
            const expectedSeries = 'Venice Mystery';
            const expectedSeriesIndex = '1';

            const html = await getHtml(filename);
            const result = getAmazonData(html);

            expect(result.seriesTitle).to.equal(expectedSeries);
            expect(result.seriesIndex).to.equal(expectedSeriesIndex);
        });
    });

    describe('when publication year is available', () => {
        it('should return the year of the publication date', async () => {
            const filename = 'a_Genevieve Cogman - The Invisible Library';
            const expectedYear = 2014;
            const html = await getHtml(filename);

            expect(getAmazonData(html).publicationYear).to.equal(expectedYear);
        });
    });

    describe('when publication year is only available in publisher data', () => {
        it('should return the year of the publication date from publisher data', async () => {
            const filename = 'a_Maria Luisa Minarelli - Murder in Venice_paperback';
            const expectedYear = 2019;
            const html = await getHtml(filename);

            expect(getAmazonData(html).publicationYear).to.equal(expectedYear);
        });
    });

    describe('when pages is available', () => {
        it('should return the number of pages', async () => {
            const filename = 'a_Genevieve Cogman - The Invisible Library';
            const expected = 337;
            const html = await getHtml(filename);

            expect(getAmazonData(html).pages).to.equal(expected);
        });

        it('should return the number of pages', async () => {
            const filename =
                'a_Jun Eishima, Yoko Taro - NieR - Automata - Short Story Long';
            const expected = 256;
            const html = await getHtml(filename);

            expect(getAmazonData(html).pages).to.equal(expected);
        });
    });

    describe('when description is available', () => {
        it('should return the description', async () => {
            const filename = 'a_John Lewis, Andrew Aydin - March - Book Three';
            const expected =
                '<div>By Fall 1963, the Civil Rights Movement is an undeniable keystone of the national conversation, and as chair of the Student Nonviolent Coordinating Committee, John Lewis is right in the thick of it. With the stakes continuing to rise, white supremacists intensify their opposition through government obstruction and civilian terrorist attacks, a supportive president is assassinated, and African-Americans across the South are still blatantly prohibited from voting. To carry out their nonviolent revolution, Lewis and an army of young activists launch a series of innovative projects, including the Freedom Vote, Mississippi Freedom Summer, and a pitched battle for the soul of the Democratic Party waged live on national television. But strategic disputes are deepening within the movement, even as 25-year-old John Lewis heads to Alabama to risk everything in a historic showdown that will shock the world.</div>\n<em></em>';
            const html = await getHtml(filename);

            expect(getAmazonData(html).description).to.equal(expected);
        });
    });

    describe('when genres is available', () => {
        it('should return the unique genres', async () => {
            const filename = 'a_Iris Johansen, Roy Johansen - Night Watch';
            const expected = [
                'Medical Thrillers',
                'Private Investigator Mysteries'
            ];
            const html = await getHtml(filename);

            expect(getAmazonData(html).genres).to.deep.equal(expected);
        });
    });

    describe('when rating is available', () => {
        it('should return rating data', async () => {
            const filename = 'a_Genevieve Cogman - The Invisible Library';
            const expected = {
                rating: 4.2,
                numberOfRatings: 2024
            };
            const html = await getHtml(filename);

            expect(getAmazonData(html).amazonRating).to.deep.equal(expected);
        });
    });

    describe("when data isn't available", () => {
        it('should return null or empty array for all property', async () => {
            const filename = 'a_Page Not Found';
            const html = await getHtml(filename);
            const expected = {
                title: null,
                author: [],
                seriesTitle: null,
                seriesIndex: null,
                pages: null,
                publicationYear: null,
                description: null,
                genres: [],
                amazonRating: { rating: null, numberOfRatings: null },
                asin: ''
            };

            expect(getAmazonData(html)).to.deep.equal(expected);
        });
    });
});
