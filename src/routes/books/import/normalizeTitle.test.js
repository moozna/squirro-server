const { expect } = require('chai');
const normalizeTitle = require('./normalizeTitle.js');

describe('normalizeTitle()', () => {
    describe('when title contains all specified word', () => {
        it('should remove "Series", "Book" and format the number', () => {
            const title =
                'Spymaster: A Thriller (The Scot Harvath Series Book 18)';
            const expected = {
                title: 'Spymaster: A Thriller',
                series: 'The Scot Harvath #18'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Book" and format the number', () => {
            const title = 'Twist and Turn (Kyle Achilles Book 4)';
            const expected = {
                title: 'Twist and Turn',
                series: 'Kyle Achilles #4'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Series" and format the number', () => {
            const title = 'Ninefox Gambit (Machineries of Empire Series 1)';
            const expected = {
                title: 'Ninefox Gambit',
                series: 'Machineries of Empire #1'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should format the number', () => {
            const title = 'The Runestone Incident (The Incident 2)';
            const expected = {
                title: 'The Runestone Incident',
                series: 'The Incident #2'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });

    describe('when title contains extra whitespaces', () => {
        it('should remove "Series", "Book" and format the number', () => {
            const title =
                'Spymaster: A Thriller (    The Scot Harvath    Series Book 18    )';
            const expected = {
                title: 'Spymaster: A Thriller',
                series: 'The Scot Harvath #18'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Book" and format the number', () => {
            const title = 'Twist and Turn (    Kyle Achilles    Book 4   )';
            const expected = {
                title: 'Twist and Turn',
                series: 'Kyle Achilles #4'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Series" and format the number', () => {
            const title =
                'Ninefox Gambit (    Machineries of Empire Series    1  )';
            const expected = {
                title: 'Ninefox Gambit',
                series: 'Machineries of Empire #1'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should format the number', () => {
            const title = 'The Runestone Incident (   The Incident    2   )';
            const expected = {
                title: 'The Runestone Incident',
                series: 'The Incident #2'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });

    describe('when title contains comma before "Series"', () => {
        it('should remove "Series", "Book" and format the number', () => {
            const title =
                'Spymaster: A Thriller (The Scot Harvath, Series Book 18)';
            const expected = {
                title: 'Spymaster: A Thriller',
                series: 'The Scot Harvath #18'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Series" and format the number', () => {
            const title = 'Ninefox Gambit (Machineries of Empire, Series 1)';
            const expected = {
                title: 'Ninefox Gambit',
                series: 'Machineries of Empire #1'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });

    describe('when title contains comma before "Book"', () => {
        it('should remove "Series", "Book" and format the number', () => {
            const title =
                'Spymaster: A Thriller (The Scot Harvath Series, Book 18)';
            const expected = {
                title: 'Spymaster: A Thriller',
                series: 'The Scot Harvath #18'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Book" and format the number', () => {
            const title = 'Twist and Turn (Kyle Achilles, Book 4)';
            const expected = {
                title: 'Twist and Turn',
                series: 'Kyle Achilles #4'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });

    describe('when title contains comma before the number', () => {
        it('should format the number', () => {
            const title = 'The Runestone Incident (The Incident, 2)';
            const expected = {
                title: 'The Runestone Incident',
                series: 'The Incident #2'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });

    describe('when title contains "#" before the number', () => {
        it('should remove "Series", "Book" and format the number', () => {
            const title =
                'Spymaster: A Thriller (The Scot Harvath Series Book #18)';
            const expected = {
                title: 'Spymaster: A Thriller',
                series: 'The Scot Harvath #18'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Book" and format the number', () => {
            const title = 'Twist and Turn (Kyle Achilles Book #4)';
            const expected = {
                title: 'Twist and Turn',
                series: 'Kyle Achilles #4'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should remove "Series" and format the number', () => {
            const title = 'Ninefox Gambit (Machineries of Empire Series #1)';
            const expected = {
                title: 'Ninefox Gambit',
                series: 'Machineries of Empire #1'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should format the number', () => {
            const title = 'The Runestone Incident (The Incident #2)';
            const expected = {
                title: 'The Runestone Incident',
                series: 'The Incident #2'
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });

    describe('when title contains multiple brackets', () => {
        describe('and the specified text is at the end of the title', () => {
            it('should remove "Series", "Book" and format the number', () => {
                const title =
                    'Fire & Blood (A Targaryen History) (A Song of Ice and Fire Series Book 1)';
                const expected = {
                    title: 'Fire & Blood (A Targaryen History)',
                    series: 'A Song of Ice and Fire #1'
                };

                expect(normalizeTitle(title)).to.deep.equal(expected);
            });

            it('should remove "Book" and format the number', () => {
                const title =
                    'Fire & Blood (A Targaryen History) (A Song of Ice and Fire Book 1)';
                const expected = {
                    title: 'Fire & Blood (A Targaryen History)',
                    series: 'A Song of Ice and Fire #1'
                };

                expect(normalizeTitle(title)).to.deep.equal(expected);
            });

            it('should remove "Series" and format the number', () => {
                const title =
                    'Fire & Blood (A Targaryen History) (A Song of Ice and Fire Series 1)';
                const expected = {
                    title: 'Fire & Blood (A Targaryen History)',
                    series: 'A Song of Ice and Fire #1'
                };

                expect(normalizeTitle(title)).to.deep.equal(expected);
            });

            it('should remove format the number', () => {
                const title =
                    'Fire & Blood (A Targaryen History) (A Song of Ice and Fire 1)';
                const expected = {
                    title: 'Fire & Blood (A Targaryen History)',
                    series: 'A Song of Ice and Fire #1'
                };

                expect(normalizeTitle(title)).to.deep.equal(expected);
            });
        });

        describe('and the specified text is not at the end of the title', () => {
            it('should return the original title', () => {
                const title =
                    'Fire & Blood (A Song of Ice and Fire Series 1) (A Targaryen History)';
                const expected = {
                    title: title,
                    series: undefined
                };

                expect(normalizeTitle(title)).to.deep.equal(expected);
            });
        });
    });

    describe('when title does not contain any matching pattern', () => {
        it('should return the original title', () => {
            const title = 'Ninefox Gambit (Machineries of Empire)';
            const expected = {
                title: title,
                series: undefined
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should return the original title', () => {
            const title =
                'The Passage: A Novel (Book One of The Passage Trilogy)';
            const expected = {
                title: title,
                series: undefined
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });

        it('should return the original title', () => {
            const title =
                'Harden (Lee Harden Series (The Remaining Universe) Book 1)';
            const expected = {
                title: title,
                series: undefined
            };

            expect(normalizeTitle(title)).to.deep.equal(expected);
        });
    });
});
