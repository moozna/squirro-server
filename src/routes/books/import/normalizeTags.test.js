const { expect } = require('chai');
const db = require('../../../db/db');
const config = require('../../../../config/config');
const normalizeTags = require('./normalizeTags.js');

describe('normalizeTags()', () => {
    beforeEach(async () => {
        await db.connect(config.dbName.test);
    });

    afterEach(async () => {
        await db.getCollection('normalizations').deleteMany({});
        await db.close();
    });

    it('is a function', () => {
        expect(typeof normalizeTags).to.equal('function');
    });

    it('does not throw when tags is undefined', async () => {
        const result = await normalizeTags();
        expect(result).to.deep.equal([]);
    });

    it('returns the same values when ignored data is not available', async () => {
        const mockTags = ['science-fiction', 'kindle', '2016'];
        const result = await normalizeTags(mockTags);

        expect(result).to.deep.equal(mockTags);
    });

    describe('when ignored data is empty', () => {
        const mockIgnoreData = {
            name: 'tags'
        };

        beforeEach(async () => {
            await db.getCollection('normalizations').insertOne(mockIgnoreData);
        });

        it('filters out tags that contain year', async () => {
            const mockTags = [
                'science-fiction',
                'read-in-2014',
                '2016',
                'books-i-own',
                '2014-reads'
            ];
            const expected = ['science-fiction', 'books-i-own'];
            const result = await normalizeTags(mockTags);

            expect(result).to.deep.equal(expected);
        });

        it('filters out tags that contain ratings', async () => {
            const mockTags = [
                'science-fiction',
                '5-stars',
                'books-i-own',
                '4-stars',
                '1-star'
            ];
            const expected = ['science-fiction', 'books-i-own'];
            const result = await normalizeTags(mockTags);

            expect(result).to.deep.equal(expected);
        });

        it('filters out tags that contain kindle', async () => {
            const mockTags = [
                'science-fiction',
                'kindle',
                'amazon',
                'kindle-ebook',
                'books-i-own',
                'amazon-favorite'
            ];
            const expected = ['science-fiction', 'books-i-own'];
            const result = await normalizeTags(mockTags);

            expect(result).to.deep.equal(expected);
        });
    });

    describe('normalization', () => {
        const mockIgnoreData = {
            name: 'tags',
            synonyms: {
                funny: ['funny', 'hilarious', 'laugh-out-loud'],
                music: ['music', 'music-related']
            },
            ignored: [
                '100-200-pages',
                'annoying-characters',
                'missing',
                'my-collection'
            ]
        };

        beforeEach(async () => {
            await db.getCollection('normalizations').insertOne(mockIgnoreData);
        });

        it('replaces synonyms with the correct value', async () => {
            const mockTags = [
                'science-fiction',
                'hilarious',
                'high-school',
                'laugh-out-loud',
                'music-related'
            ];
            const expected = [
                'science-fiction',
                'funny',
                'high-school',
                'music'
            ];
            const result = await normalizeTags(mockTags);

            expect(result).to.deep.equal(expected);
        });

        it('filters out ignored values', async () => {
            const mockTags = [
                'science-fiction',
                '100-200-pages',
                'annoying-characters',
                'high-school',
                'missing',
                'my-collection'
            ];
            const expected = ['science-fiction', 'high-school'];
            const result = await normalizeTags(mockTags);

            expect(result).to.deep.equal(expected);
        });
    });
});
