const findSynonym = (value, synonyms) => {
    const entries = Object.entries(synonyms);

    for (let i = 0; i < entries.length; i++) {
        const [currentSynonym, currentOptions] = entries[i];

        if (currentOptions.includes(value)) {
            return currentSynonym;
        }
    }

    return null;
};

const replaceSynonyms = (values = [], synonyms = {}) =>
    values.reduce((finalValues, currentValue) => {
        const foundSynonym = findSynonym(currentValue, synonyms);

        if (!foundSynonym) {
            finalValues.push(currentValue);
        } else if (!finalValues.includes(foundSynonym)) {
            finalValues.push(foundSynonym);
        }

        return finalValues;
    }, []);

module.exports = replaceSynonyms;
