//    Removes:
//         Series Book
//         Book
//         Series

//     Format Title:
//         Title <> SeriesName #Num
//         Title <> --

const PATTERN_SERIES_BOOK = /\(([^\)]*?),? Series,?\s*Book[\s\,#]*(\d+)\s*\)$/i;
const PATTERN_BOOK = /\(([^\)]*?),? Book[\s\,#]*(\d+)\s*\)$/i;
const PATTERN_SERIES = /\(([^\)]*?),? Series[\s\,#]*(\d+)\s*\)$/i;
const PATTERN_NUMBER = /\(([^\)]*?),?[\s\,#]*(\d+)\s*\)$/i;

const normalizeTitle = originalTitle => {
    let title, series;

    const result =
        PATTERN_SERIES_BOOK.exec(originalTitle) ||
        PATTERN_BOOK.exec(originalTitle) ||
        PATTERN_SERIES.exec(originalTitle) ||
        PATTERN_NUMBER.exec(originalTitle);

    if (result) {
        title = originalTitle.slice(0, result.index).trim();
        series = result[1].trim() + ' #' + result[2].trim();
    }

    return {
        title: title || originalTitle,
        series: series
    };
};

module.exports = normalizeTitle;
