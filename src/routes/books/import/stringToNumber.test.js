const { expect } = require('chai');
const stringToNumber = require('./stringToNumber.js');

describe('stringToNumber()', () => {
    it('is a function', () => {
        expect(typeof stringToNumber).to.equal('function');
    });

    describe('does not throw', () => {
        it('when value is undefined', () => {
            const testValue = undefined;
            expect(stringToNumber(testValue)).to.be.null;
        });

        it('when value is null', () => {
            const testValue = null;
            expect(stringToNumber(testValue)).to.be.null;
        });
    });

    describe('returns null', () => {
        it('when value is an empty string', () => {
            const testValue = '';
            expect(stringToNumber(testValue)).to.be.null;
        });
    });

    describe('returns a number', () => {
        it('when value is zero', () => {
            const testValue = '0';
            expect(stringToNumber(testValue)).to.be.equal(0);
        });

        it('when value is an integer', () => {
            const testValue = '1200';
            expect(stringToNumber(testValue)).to.be.equal(1200);
        });

        it('when value is a decimal number', () => {
            const testValue = '4.55';
            expect(stringToNumber(testValue)).to.be.equal(4.55);
        });
    });
});
