function stringToNumber(val) {
    if (!val) {
        return null;
    }
    return parseFloat(val);
}

module.exports = stringToNumber;
