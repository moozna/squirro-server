const fetch = require('node-fetch');
const cheerio = require('cheerio');
const normalizeTags = require('./normalizeTags');

const SELECTORS = {
    shelfName: '.left .mediumText'
};

function getListopiaTags(html) {
    const $ = cheerio.load(html);
    return $(SELECTORS.shelfName)
        .map((i, elem) => {
            return $(elem).text();
        })
        .get();
}

async function downloadListopiaTags(id) {
    const url = `https://www.goodreads.com/book/shelves/${id}`;

    const response = await fetch(url);
    const html = await response.text();

    const shelves = await getListopiaTags(html);
    return normalizeTags(shelves);
}

module.exports = downloadListopiaTags;
