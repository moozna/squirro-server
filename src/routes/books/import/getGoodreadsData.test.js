const { expect } = require('chai');
const db = require('../../../db/db');
const config = require('../../../../config/config');
const getGooreadsData = require('./getGoodreadsData.js');
const getHtml = require('../../../../test/getHtml');

describe('getGoodreadsData()', () => {
    beforeEach(async () => {
        await db.connect(config.dbName.test);
    });

    afterEach(async () => {
        await db.close();
    });

    describe('when title is available', () => {
        it('should return the title', async () => {
            const filename = 'g_Stephen King - End of Watch';
            const expected = 'End of Watch';
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.title).to.equal(expected);
        });
    });

    describe('when there is one author', () => {
        it('should return one author', async () => {
            const filename = 'g_Pierce Brown - Morning Star';
            const expected = ['Pierce Brown'];
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.author).to.deep.equal(expected);
        });
    });

    describe('when there are multiple authors', () => {
        it('should return multiple authors', async () => {
            const filename = 'g_Iris Johansen, Roy Johansen - Night Watch';
            const expected = ['Iris Johansen', 'Roy Johansen'];
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.author).to.deep.equal(expected);
        });
    });

    describe('when series data is available', () => {
        it('should return series data', async () => {
            const filename = 'g_Stephen King - End of Watch';
            const expectedSeries = 'Bill Hodges Trilogy';
            const expectedSeriesIndex = '3';

            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.seriesTitle).to.equal(expectedSeries);
            expect(result.seriesIndex).to.equal(expectedSeriesIndex);
        });
    });

    describe('when publication date is available', () => {
        describe('when first publication date is available', () => {
            it('should return the year of the first publication date', async () => {
                const filename = 'g_J.F. Penn - Stone of Fire';
                const expected = 2011;
                const html = await getHtml(filename);
                const result = await getGooreadsData(html);

                expect(result.publicationYear).to.equal(expected);
            });
        });

        describe("when first publication date isn't available", () => {
            it('should return the year of the publication date', async () => {
                const filename = 'g_Stephen King - End of Watch';
                const expected = 2016;
                const html = await getHtml(filename);
                const result = await getGooreadsData(html);

                expect(result.publicationYear).to.equal(expected);
            });
        });
    });

    describe('when pages is available', () => {
        it('should return the number of pages', async () => {
            const filename = 'g_Pierce Brown - Morning Star';
            const expected = 524;
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.pages).to.equal(expected);
        });
    });

    describe('when long description is available', () => {
        it('should return the description', async () => {
            const filename = 'g_J.F. Penn - Stone of Fire';
            const expected =
                '<strong>\n  <em>A power kept secret for 2000 years. A woman who stands to lose everything.</em>\n</strong><br><br><strong>India.</strong> When a nun is burned alive on the sacred ghats of Varanasi, and the stone she carried is stolen, an international hunt is triggered for the relics of the early church.<br><br>Forged in the fire and blood of martyrs, the Pentecost stones have been handed down through generations of Keepers who kept their power and locations secret.<br><br>Until now.<br><br>The Keepers are being murdered, the stones stolen by those who would use them for evil in a world transformed by religious fundamentalism.<br><br>Oxford University psychologist Morgan Sierra is forced into the search when her sister and niece are held hostage. She is helped by Jake Timber from the mysterious ARKANE, a British government agency specializing in paranormal and religious experience. Morgan must risk her own life to save her family, but will she ultimately be betrayed?<br><br>From ancient Christian sites in Spain, Italy, and Israel to the far reaches of Iran and Tunisia, Morgan and Jake must track down the stones through the myths of the early church in a race against time before a new Pentecost is summoned, this time powered by the fires of evil.<br><br>The first in the ARKANE series, <em>Stone of Fire</em> is a fast-paced, action-packed thriller that explores the edges of faith against a backdrop of early Christian history, archaeology and psychology.';
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.description).to.equal(expected);
        });
    });

    describe('when only short description is available', () => {
        it('should return the description', async () => {
            const filename = 'g_John Lewis, Andrew Aydin - March - Book Three';
            const expected =
                'Welcome to the stunning conclusion of the award-winning and best-selling MARCH trilogy. Congressman John Lewis, an American icon and one of the key figures of the civil rights movement, joins co-writer Andrew Aydin and artist Nate Powell to bring the lessons of history to vivid life for a new generation, urgently relevant for today\'s world.';
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.description).to.equal(expected);
        });
    });

    describe('when genres is available', () => {
        it('should return the genres', async () => {
            const filename = 'g_Pierce Brown - Morning Star';
            const expected = [
                'Science Fiction',
                'Fantasy',
                'Fiction',
                'Science Fiction > Dystopia',
                'Young Adult'
            ];
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.genres).to.deep.equal(expected);
        });
    });

    describe('when rating is available', () => {
        it('should return rating data', async () => {
            const filename = 'g_Pierce Brown - Morning Star';
            const expected = {
                rating: 4.49,
                numberOfRatings: 95846
            };
            const html = await getHtml(filename);
            const result = await getGooreadsData(html);

            expect(result.goodreadsRating).to.deep.equal(expected);
        });
    });

    describe("when data isn't available", () => {
        it('should return null or empty array for all property', async () => {
            const filename = 'g_Page Not Found';
            const html = await getHtml(filename);
            const expected = {
                title: null,
                author: [],
                seriesTitle: null,
                seriesIndex: null,
                publicationYear: null,
                pages: null,
                description: null,
                genres: [],
                originalGenres: [],
                goodreadsRating: {
                    rating: null,
                    numberOfRatings: null
                }
            };
            const result = await getGooreadsData(html);

            expect(result).to.deep.equal(expected);
        });
    });
});
