const db = require('../../../db/db');
const replaceSynonyms = require('./replaceSynonyms');

const isContainsYear = text => {
    const yearPattern = /\d{4}/;
    return yearPattern.test(text);
};

const isContainsRating = text => {
    const ratingPattern = /\d{1}-star(s)?/;
    return ratingPattern.test(text);
};

const isContainsKindle = text => {
    const kindlePattern = /kindle|amazon/i;
    return kindlePattern.test(text);
};

const isTagNeeded = (tag, ignored) => {
    if (
        ignored.includes(tag) ||
        isContainsYear(tag) ||
        isContainsRating(tag) ||
        isContainsKindle(tag)
    ) {
        return false;
    }

    return true;
};

async function normalizeTags(shelfNames = []) {
    const ignoredData = await db
        .getCollection('normalizations')
        .findOne({ name: 'tags' });

    if (!ignoredData) {
        return shelfNames;
    }

    const { ignored = [], synonyms } = ignoredData;

    const filteredTags = shelfNames.filter(tag => isTagNeeded(tag, ignored));
    return replaceSynonyms(filteredTags, synonyms);
}

module.exports = normalizeTags;
