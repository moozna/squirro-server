const { expect } = require('chai');
const db = require('../../../db/db');
const config = require('../../../../config/config');
const normalizeGenres = require('./normalizeGenres.js');

describe('normalizeGenres()', () => {
    beforeEach(async () => {
        await db.connect(config.dbName.test);
    });

    afterEach(async () => {
        await db.getCollection('normalizations').deleteMany({});
        await db.close();
    });

    it('is a function', () => {
        expect(typeof normalizeGenres).to.equal('function');
    });

    it('does not throw when genres is undefined', async () => {
        const result = await normalizeGenres();
        expect(result).to.deep.equal([]);
    });

    it('returns the same values when ignored data is not available', async () => {
        const mockGenres = [
            'Fantasy > Paranormal',
            'Thriller > Mystery Thriller',
            'Science Fiction > Dystopia'
        ];
        const result = await normalizeGenres(mockGenres);

        expect(result).to.deep.equal(mockGenres);
    });

    it('returns separated values when ignored data is empty', async () => {
        const mockIgnoreData = {
            name: 'genres'
        };
        await db.getCollection('normalizations').insertOne(mockIgnoreData);

        const mockGenres = [
            'Fantasy > Paranormal',
            'Thriller > Mystery Thriller',
            'Science Fiction > Dystopia'
        ];
        const expected = [
            'Fantasy',
            'Paranormal',
            'Thriller',
            'Mystery Thriller',
            'Science Fiction',
            'Dystopia'
        ];
        const result = await normalizeGenres(mockGenres);

        expect(result).to.deep.equal(expected);
    });

    describe('normalization', () => {
        const mockIgnoreData = {
            name: 'genres',
            mappings: {
                'Paranormal Urban Fantasy': 'Urban Fantasy',
                'Space > Space Opera': 'Space Opera'
            },
            synonyms: {
                Mystery: ['Mystery', 'Cozy Mystery', 'Mystery Thriller'],
                'Post Apocalyptic': ['Post Apocalyptic', 'Apocalyptic']
            },
            ignored: ['Amazon', 'Business', 'Novella']
        };

        beforeEach(async () => {
            await db.getCollection('normalizations').insertOne(mockIgnoreData);
        });

        it('replaces values with the correct mapping', async () => {
            const mockGenres = [
                'Paranormal Urban Fantasy',
                'Science Fiction',
                'Space > Space Opera'
            ];
            const expected = [
                'Urban Fantasy',
                'Science Fiction',
                'Space Opera'
            ];
            const result = await normalizeGenres(mockGenres);

            expect(result).to.deep.equal(expected);
        });

        it('replaces synonyms with the correct value', async () => {
            const mockGenres = [
                'Fantasy',
                'Mystery > Cozy Mystery',
                'Apocalyptic > Post Apocalyptic'
            ];
            const expected = ['Fantasy', 'Mystery', 'Post Apocalyptic'];
            const result = await normalizeGenres(mockGenres);

            expect(result).to.deep.equal(expected);
        });

        it('filters out ignored values', async () => {
            const mockGenres = ['Business > Amazon', 'Fantasy', 'Novella'];
            const expected = ['Fantasy'];
            const result = await normalizeGenres(mockGenres);

            expect(result).to.deep.equal(expected);
        });
    });
});
