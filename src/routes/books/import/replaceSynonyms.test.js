const { expect } = require('chai');
const replaceSynonyms = require('./replaceSynonyms.js');

const mockSynonyms = {
    Mystery: ['Mystery', 'Cozy Mystery', 'Mystery Thriller'],
    'Post Apocalyptic': ['Post Apocalyptic', 'Apocalyptic']
};

describe('replaceSynonyms()', () => {
    it('is a function', () => {
        expect(typeof replaceSynonyms).to.equal('function');
    });

    it('does not throw when genres and synonyms are undefined', async () => {
        const result = await replaceSynonyms();
        expect(result).to.deep.equal([]);
    });

    it('returns the same values, when synonyms are not found', () => {
        const testValues = ['Fantasy', 'Music', 'Science Fiction'];

        expect(replaceSynonyms(testValues, mockSynonyms)).to.deep.equal(
            testValues
        );
    });

    it('replaces synonyms with the correct value', () => {
        const testValues = ['Fantasy', 'Cozy Mystery', 'Apocalyptic'];
        const expectedValues = ['Fantasy', 'Mystery', 'Post Apocalyptic'];

        expect(replaceSynonyms(testValues, mockSynonyms)).to.deep.equal(
            expectedValues
        );
    });

    it('does not duplicates values when replacing synonyms', () => {
        const testValues = ['Mystery', 'Cozy Mystery', 'Apocalyptic'];
        const expectedValues = ['Mystery', 'Post Apocalyptic'];

        expect(replaceSynonyms(testValues, mockSynonyms)).to.deep.equal(
            expectedValues
        );
    });
});
