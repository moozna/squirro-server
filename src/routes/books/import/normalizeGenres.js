const db = require('../../../db/db');
const replaceSynonyms = require('./replaceSynonyms');

async function normalizeGenres(genres = []) {
    const ignoredData = await db
        .getCollection('normalizations')
        .findOne({ name: 'genres' });

    if (!ignoredData) {
        return genres;
    }

    const { mappings = {}, ignored = [], synonyms } = ignoredData;

    let filteredGenres = genres.reduce((result, currentGenre) => {
        const normalizedGenre = mappings[currentGenre] || currentGenre;
        const genreParts = normalizedGenre.split(' > ');
        genreParts.forEach(genre => {
            if (!result.includes(genre) && !ignored.includes(genre)) {
                result.push(genre);
            }
        });
        return result;
    }, []);

    filteredGenres = replaceSynonyms(filteredGenres, synonyms);
    return filteredGenres;
}

module.exports = normalizeGenres;
