const crypto = require('crypto');
const sharp = require('sharp');
const {
    renameFile,
    deleteFile,
    getFullFilePath
} = require('../../utils/fileUtils');

const FILE_EXTENSIONS = {
    'image/png': '.png',
    'image/jpeg': '.jpg',
    'image/jpg': '.jpg'
};

const COVER_PATH = '/images/covers';
const SMALL_COVER_PATH = '/images/smallcovers';

function getOriginalFileName(name) {
    const fileExtensionPattern = /\.[^/.]+$/;
    return name.replace(fileExtensionPattern, '');
}

function getRandomId(length = 4) {
    return crypto
        .randomBytes(16)
        .toString('hex')
        .slice(0, length);
}

function createFileName(image, newFileName) {
    const fileName = newFileName || getOriginalFileName(image.originalname);
    const fileExtension = FILE_EXTENSIONS[image.mimetype];
    return `${fileName}--${getRandomId()}${fileExtension}`;
}

function resizeImage(currentPath, targetPath) {
    return sharp(currentPath)
        .resize({
            width: 140,
            kernel: sharp.kernel.lanczos3,
            fastShrinkOnLoad: false
        })
        .jpeg({
            quality: 80,
            chromaSubsampling: '4:2:0'
        })
        .toFile(targetPath);
}

function saveImage(image, newFileName, targetBase, resizedTargetBase) {
    return new Promise((resolve, reject) => {
        const currentPath = image.path;
        const fileName = createFileName(image, newFileName);
        const resizedTargetPath = `${resizedTargetBase}/${fileName}`;
        const targetPath = `${targetBase}/${fileName}`;

        resizeImage(currentPath, getFullFilePath(resizedTargetPath))
            .then(() => renameFile(currentPath, getFullFilePath(targetPath)))
            .then(() => {
                resolve({
                    url: targetPath,
                    thumbnail: resizedTargetPath
                });
            })
            .catch(error => {
                deleteFile(getFullFilePath(resizedTargetPath));
                reject(error);
            });
    });
}

function deleteAllTypeOfCurrentCover(cover) {
    return Object.values(cover).map(filePath => {
        const coverPath = getFullFilePath(filePath);
        return new Promise((resolve, reject) => {
            deleteFile(coverPath)
                .then(() => resolve(filePath))
                .catch(() => reject(coverPath));
        });
    });
}

function deleteCovers(covers) {
    return covers.reduce((processes, currentCover) => {
        return processes.concat(deleteAllTypeOfCurrentCover(currentCover));
    }, []);
}

function deleteAllCovers(covers) {
    return Promise.allSettled(deleteCovers(covers)).then(results => {
        const missingFilePaths = results.reduce(
            (missingFiles, currentResult) => {
                if (currentResult.status === 'rejected') {
                    missingFiles.push(currentResult.reason);
                }
                return missingFiles;
            },
            []
        );

        if (missingFilePaths.length) {
            throw new Error(`Missing files: ${missingFilePaths.join(', ')}`);
        }
    });
}

async function uploadCoverImage(req, res) {
    const { files = [] } = req;
    const { asin } = req.body;
    const processes = files.map(currentFile =>
        saveImage(currentFile, asin, COVER_PATH, SMALL_COVER_PATH)
    );

    const results = await Promise.all(processes);
    res.json(results);
}

async function deleteCoverImage(req, res) {
    const { files = [] } = req.body;
    const results = await deleteAllCovers(files);
    res.json(results);
}

module.exports = {
    uploadCoverImage,
    deleteCoverImage,
    deleteAllCovers
};
