const { expect } = require('chai');
const sinon = require('sinon');
const app = require('../../app');
const request = require('supertest')(app);
const fs = require('fs');
const sharp = require('sharp');
const {
    copyFile,
    deleteFile,
    getFullFilePath
} = require('../../utils/fileUtils');

const testImage = './test/testImage.jpg';

describe('POST /api/images', () => {
    describe('POST /images/covers', () => {
        it('should return 200 and file informations', async () => {
            const { body } = await request
                .post('/api/images/covers')
                .field('Content-Type', 'multipart/form-data')
                .field('asin', 'testAsin')
                .attach('images', testImage)
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.length).to.be.equal(1);

            await Promise.all([
                deleteFile(getFullFilePath(body[0].url)),
                deleteFile(getFullFilePath(body[0].thumbnail))
            ]);
        });

        it('should use the original filename when asin number is not specified', async () => {
            const { body } = await request
                .post('/api/images/covers')
                .field('Content-Type', 'multipart/form-data')
                .attach('images', testImage)
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.length).to.be.equal(1);
            expect(body[0].url).to.match(/testImage--.{4}\.jpg/);
            expect(body[0].thumbnail).to.match(/testImage--.{4}\.jpg/);

            await Promise.all([
                deleteFile(getFullFilePath(body[0].url)),
                deleteFile(getFullFilePath(body[0].thumbnail))
            ]);
        });

        it('should not throw when files is empty', async () => {
            const { body } = await request
                .post('/api/images/covers')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body.length).to.be.equal(0);
        });

        it('should return 500 and error message when resizing throws an error', async () => {
            const expectedError = new Error('Resize error');
            const jimpStub = sinon
                .stub(sharp.prototype, 'resize')
                .throws(expectedError);
            const { body } = await request
                .post('/api/images/covers')
                .field('Content-Type', 'multipart/form-data')
                .field('asin', 'testAsin')
                .attach('images', testImage)
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('stack').and.to.includes(expectedError);
            jimpStub.restore();
        });

        it('should return 500 and error message when renaming throws an error', async () => {
            const expectedError = new Error('Rename error');
            const renameStub = sinon.stub(fs, 'rename').callsFake((currentPath, targetPath, callback) =>
                callback(expectedError)
            );
            const { body } = await request
                .post('/api/images/covers')
                .field('Content-Type', 'multipart/form-data')
                .field('asin', 'testAsin')
                .attach('images', testImage)
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('stack').and.to.includes(expectedError);
            renameStub.restore();
            await deleteFile(getFullFilePath('/uploads/testImage.jpg'));
        });
    });

    describe('POST /api/images/covers/deletes', () => {
        const coverPath = '/images/covers/testImage.jpg';
        const smallCoverPath = '/images/smallcovers/testImage.jpg';
        const mockFiles = [
            {
                url: coverPath,
                thumbnail: smallCoverPath
            }
        ];

        it('should return 200', async () => {
            await copyFile(testImage, getFullFilePath(coverPath));
            await copyFile(testImage, getFullFilePath(smallCoverPath));
            await request
                .post('/api/images/covers/deletes')
                .type('json')
                .send({ files: mockFiles })
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
        });

        it('should not throw when files is empty', async () => {
            await request
                .post('/api/images/covers/deletes')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
        });

        it('should return 500 and error message when file does not exist', async () => {
            const expectedError = `Missing files: ./public${coverPath}, ./public${smallCoverPath}`;
            const unlinkStub = sinon.stub(fs, 'unlink').callsFake((path, callback) =>
                callback(expectedError)
            );
            const { body } = await request
                .post('/api/images/covers/deletes')
                .type('json')
                .send({ files: mockFiles })
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('message').and.to.be.equal(expectedError);
            unlinkStub.restore();
        });
    });
});
