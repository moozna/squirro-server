const express = require('express');
const multer = require('multer');
const asyncErrorHandler = require('../../middlewares/asyncErrorHandler');
const { uploadCoverImage, deleteCoverImage } = require('./imagesController');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => cb(null, 'public/uploads/'),
    filename: (req, file, cb) => cb(null, file.originalname)
});

const upload = multer({ storage });

router.post('/covers', upload.array('images'), asyncErrorHandler(uploadCoverImage));
router.post('/covers/deletes', asyncErrorHandler(deleteCoverImage));

module.exports = router;
