const { expect } = require('chai');
const app = require('../../app');
const request = require('supertest')(app);

describe('GET /', () => {
    it('responds with 200 status code and json', async () => {
        const { body } = await request
            .get('/api/home')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(body).to.have.property('title').and.equal('Express server is running.');
    });
});
