const request = require('supertest');
const express = require('express');
const HTTPStatus = require('http-status');
const notFoundHandler = require('./notFoundHandler');

describe('notFoundHandler()', () => {
    let app;

    const createApp = () => {
        app = express();
        app.get('/ok', (req, res) => res.json({ ok: true }));
        app.use(notFoundHandler);
    };

    beforeEach(() => {
        createApp();
    });

    it('should not return 404', async () => {
        await request(app)
            .get('/ok')
            .expect(HTTPStatus.OK, { ok: true });
    });

    it('should return 404', async () => {
        await request(app)
            .get('/404')
            .expect(HTTPStatus.NOT_FOUND, { status: HTTPStatus.NOT_FOUND, message: 'Route not found' });
    });
});
