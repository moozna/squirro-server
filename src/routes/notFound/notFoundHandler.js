const HTTPStatus = require('http-status');

function notFoundHandler(req, res) {
    res.status(HTTPStatus.NOT_FOUND);
    res.json({
        status: HTTPStatus.NOT_FOUND,
        message: 'Route not found'
    });
}

module.exports = notFoundHandler;
