const Joi = require('joi');

const sortingFilterSchema = Joi.object({
    sort: Joi.string().valid('name', 'count')
});

const updatedShelfSchema = Joi.object({
    shelfName: Joi.string()
});

module.exports = {
    sortingFilterSchema,
    updatedShelfSchema
};
