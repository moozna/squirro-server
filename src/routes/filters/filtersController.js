const db = require('../../db/db');
const { filteringQuery } = require('../books/queries');
const {
    COLLATION,
    sortParameter,
    uniqueValuesQuery,
    rangeQuery,
    getFirstElement
} = require('./queries');

async function getAvailableFilters(req, res) {
    const query = filteringQuery(req.query);
    const filters = await db.getCollection()
        .aggregate([
            {
                $match: query
            },
            {
                $facet: {
                    genres: [
                        { $unwind: '$genres' },
                        { $group: { _id: '$genres', count: { $sum: 1 } } },
                        { $sort: { _id: 1 } }
                    ],
                    tags: [
                        { $unwind: '$tags' },
                        { $group: { _id: '$tags', count: { $sum: 1 } } },
                        { $sort: { _id: 1 } }
                    ]
                }
            }
        ], COLLATION)
        .toArray();
    res.json(filters[0]);
}

async function getAvailableRanges(req, res) {
    const {
        genres,
        tags,
        excludedTags,
        rating,
        amazonRating,
        goodreadsRating,
        interestRange,
        readStatus,
        search,
        shelf
    } = req.query;

    const filters = {
        genres,
        tags,
        excludedTags,
        rating,
        amazonRating,
        goodreadsRating,
        interestRange,
        readStatus,
        search,
        shelf
    };
    const query = filteringQuery(filters);
    const ranges = await db.getCollection()
        .aggregate([
            {
                $match: query
            },
            {
                $facet: {
                    year: rangeQuery('publicationYear'),
                    pages: rangeQuery('pages')
                }
            },
            {
                $addFields: {
                    year: getFirstElement('year'),
                    pages: getFirstElement('pages')
                }
            }
        ], COLLATION)
        .toArray();
    res.json(ranges[0]);
}

async function getAllGenres(req, res) {
    const genres = await db.getCollection()
        .aggregate([
            { $unwind: '$genres' },
            ...uniqueValuesQuery('genres')
        ], COLLATION)
        .toArray();
    res.json(genres);
}

async function getAllTags(req, res) {
    const tags = await db.getCollection()
        .aggregate([
            { $unwind: '$tags' },
            ...uniqueValuesQuery('tags')
        ], COLLATION)
        .toArray();
    res.json(tags);
}

async function getAllAuthors(req, res) {
    const authors = await db.getCollection()
        .aggregate([
            { $unwind: '$author' },
            ...uniqueValuesQuery('author')
        ], COLLATION)
        .toArray();
    res.json(authors);
}

async function getAllSeries(req, res) {
    const series = await db.getCollection()
        .aggregate([
            { $match: { seriesTitle: { $exists: true, $ne: null } } },
            ...uniqueValuesQuery('seriesTitle')
        ], COLLATION)
        .toArray();
    res.json(series);
}

async function getAllShelves(req, res) {
    const shelves = await db.getCollection()
        .aggregate([
            { $unwind: '$shelves' },
            ...uniqueValuesQuery('shelves')
        ], COLLATION)
        .toArray();
    res.json(shelves);
}

async function getAllGenresWithCount(req, res) {
    const { sort = 'name' } = req.query;
    const sorting = sortParameter(sort);

    const genres = await db.getCollection()
        .aggregate([
            { $project: { _id: 0, genres: 1 } },
            { $unwind: '$genres' },
            { $group: { _id: '$genres', count: { $sum: 1 } } },
            sorting
        ], COLLATION)
        .toArray();
    res.json(genres);
}

async function getAllTagsWithCount(req, res) {
    const { sort = 'name' } = req.query;
    const sorting = sortParameter(sort);

    const tags = await db.getCollection()
        .aggregate([
            { $project: { _id: 0, tags: 1 } },
            { $unwind: '$tags' },
            { $group: { _id: '$tags', count: { $sum: 1 } } },
            sorting
        ], COLLATION)
        .toArray();
    res.json(tags);
}

async function getAllAuthorsWithCount(req, res) {
    const { sort = 'name' } = req.query;
    const sorting = sortParameter(sort);

    const genres = await db.getCollection()
        .aggregate([
            { $project: { _id: 0, author: 1 } },
            { $unwind: '$author' },
            { $group: { _id: '$author', count: { $sum: 1 } } },
            sorting
        ], COLLATION)
        .toArray();
    res.json(genres);
}

async function getAllSeriesWithCount(req, res) {
    const { sort = 'name' } = req.query;
    const sorting = sortParameter(sort);

    const genres = await db.getCollection()
        .aggregate([
            { $match: { seriesTitle: { $exists: true, $ne: null } } },
            { $group: { _id: '$seriesTitle', count: { $sum: 1 } } },
            sorting
        ], COLLATION)
        .toArray();
    res.json(genres);
}

async function getAllShelvesWithCount(req, res) {
    const { sort = 'name' } = req.query;
    const sorting = sortParameter(sort);

    const shelves = await db.getCollection()
        .aggregate([
            { $project: { _id: 0, shelves: 1 } },
            { $unwind: '$shelves' },
            { $group: { _id: '$shelves', count: { $sum: 1 } } },
            sorting
        ], COLLATION)
        .toArray();
    res.json(shelves);
}

async function updateShelf(req, res) {
    const { name } = req.params;
    const { shelfName } = req.body;
    await db.getCollection().updateMany(
        { shelves: name },
        { $set: { 'shelves.$': shelfName } }
    );
    res.send(`Shelf ${name} was successfully renamed!`);
}


async function deleteShelf(req, res) {
    const { name } = req.params;
    await db.getCollection().updateMany(
        { shelves: name },
        { $pull: { shelves: name } }
    );
    res.send(`Shelf '${name}' was successfully deleted!`);
}

module.exports = {
    getAvailableFilters,
    getAvailableRanges,
    getAllGenres,
    getAllTags,
    getAllAuthors,
    getAllSeries,
    getAllShelves,
    getAllGenresWithCount,
    getAllTagsWithCount,
    getAllAuthorsWithCount,
    getAllSeriesWithCount,
    getAllShelvesWithCount,
    updateShelf,
    deleteShelf
};
