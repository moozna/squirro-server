const express = require('express');
const {
    getAvailableFilters,
    getAvailableRanges,
    getAllGenres,
    getAllTags,
    getAllAuthors,
    getAllSeries,
    getAllShelves,
    getAllGenresWithCount,
    getAllTagsWithCount,
    getAllAuthorsWithCount,
    getAllSeriesWithCount,
    getAllShelvesWithCount,
    updateShelf,
    deleteShelf
} = require('./filtersController');
const { bookFilterSchema } = require('../books/booksModel');
const { sortingFilterSchema, updatedShelfSchema } = require('./filtersModel');
const asyncErrorHandler = require('../../middlewares/asyncErrorHandler');
const { validateQuery, validateBody } = require('../../middlewares/validation');

const router = express.Router();

router.get('/', validateQuery(bookFilterSchema), asyncErrorHandler(getAvailableFilters));
router.get('/ranges', validateQuery(bookFilterSchema), asyncErrorHandler(getAvailableRanges));
router.get('/genres', asyncErrorHandler(getAllGenres));
router.get('/genres/stat', validateQuery(sortingFilterSchema), asyncErrorHandler(getAllGenresWithCount));
router.get('/tags', asyncErrorHandler(getAllTags));
router.get('/tags/stat', validateQuery(sortingFilterSchema), asyncErrorHandler(getAllTagsWithCount));
router.get('/authors', asyncErrorHandler(getAllAuthors));
router.get('/authors/stat', validateQuery(sortingFilterSchema), asyncErrorHandler(getAllAuthorsWithCount));
router.get('/series', asyncErrorHandler(getAllSeries));
router.get('/series/stat', validateQuery(sortingFilterSchema), asyncErrorHandler(getAllSeriesWithCount));
router.get('/shelves', asyncErrorHandler(getAllShelves));
router.get('/shelves/stat', validateQuery(sortingFilterSchema), asyncErrorHandler(getAllShelvesWithCount));
router.put('/shelves/:name', validateBody(updatedShelfSchema), asyncErrorHandler(updateShelf));
router.delete('/shelves/:name', asyncErrorHandler(deleteShelf));

module.exports = router;
