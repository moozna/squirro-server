const COLLATION = { collation: { locale: 'en' } };

const sortParameter = sort => {
    if (sort === 'count') {
        return { $sort: { count: -1, _id: 1 } };
    }

    return { $sort: { _id: 1 } };
};

const uniqueValuesQuery = fieldName => [
    { $group: { _id: `$${fieldName}` } },
    { $sort: { _id: 1 } },
    { $project: { _id: 0, name: '$_id' } }
];

const rangeQuery = fieldName => [
    {
        $group: {
            _id: null,
            min: { $min: `$${fieldName}` },
            max: { $max: `$${fieldName}` }
        }
    },
    { $project: { _id: 0 } }
];

const getFirstElement = fieldName => ({
    $arrayElemAt: [`$${fieldName}`, 0]
});

module.exports = {
    COLLATION,
    sortParameter,
    uniqueValuesQuery,
    rangeQuery,
    getFirstElement
};
