const { expect } = require('chai');
const sinon = require('sinon');
const db = require('../../db/db');
const config = require('../../../config/config');
const app = require('../../app');
const request = require('supertest')(app);

const mockBooks = [
    {
        author: ['Kristin Cashore'],
        seriesTitle: 'Graceling Realm',
        genres: ['action', 'Romance', 'Thriller'],
        tags: ['read', 'free book', 'favourite author'],
        publicationYear: 2013,
        pages: 471,
        shelves: ['Best Historical Fiction', 'Best Book Cover Art', 'Funny as Hell!']
    },
    {
        author: ['Stephen King', 'Steven Erikson'],
        seriesTitle: 'Some Quiet Place',
        genres: ['Horror', 'Sci-fi', 'Fantasy'],
        tags: ['Want to read', 'on kindle', 'free book'],
        publicationYear: 2008,
        pages: 331,
        shelves: ['Best Twists', 'Best Horror Novels']
    },
    {
        author: ['Kristin Cashore', 'Cassandra Clare'],
        seriesTitle: 'Graceling Realm',
        genres: ['Adventure', 'Drama'],
        tags: ['Want to read', 'on kindle', 'owned'],
        publicationYear: 1995,
        pages: 55,
        shelves: ['Funny as Hell!', 'Best Book Cover Art']
    },
    {
        author: ['Stephen King'],
        seriesTitle: 'Some Quiet Place',
        genres: ['Sci-fi', 'Thriller'],
        tags: ['read', 'on kindle', 'free book', 'favourite author'],
        publicationYear: 1999,
        pages: 569,
        shelves: ['Best Horror Novels', 'Thrillers You Must Read!', 'Best Twists']
    },
    {
        author: ['Steven Erikson'],
        seriesTitle: 'Above World',
        genres: ['Fantasy', 'Horror'],
        tags: ['read', 'owned', 'maybe'],
        publicationYear: 1966,
        pages: 781,
        shelves: ['Best Book Cover Art', 'The Best Urban Fantasy']
    }
];

describe('filtersController', () => {
    beforeEach(async () => {
        await db.connect(config.dbName.test);
        await db.getCollection().insertMany(mockBooks);
    });

    afterEach(async () => {
        await db.getCollection().deleteMany({});
        await db.close();
    });

    describe('Filters', () => {
        it('should return all filters', async () => {
            const expectedFilters = {
                genres: [
                    { _id: 'action', count: 1 },
                    { _id: 'Adventure', count: 1 },
                    { _id: 'Drama', count: 1 },
                    { _id: 'Fantasy', count: 2 },
                    { _id: 'Horror', count: 2 },
                    { _id: 'Romance', count: 1 },
                    { _id: 'Sci-fi', count: 2 },
                    { _id: 'Thriller', count: 2 }
                ],
                tags: [
                    { _id: 'favourite author', count: 2 },
                    { _id: 'free book', count: 3 },
                    { _id: 'maybe', count: 1 },
                    { _id: 'on kindle', count: 3 },
                    { _id: 'owned', count: 2 },
                    { _id: 'read', count: 3 },
                    { _id: 'Want to read', count: 2 }
                ]
            };

            const { body } = await request.get('/api/filters')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedFilters);
        });

        it('should return only the available filters based on the request', async () => {
            const expectedFilters = {
                genres: [
                    { _id: 'Fantasy', count: 2 },
                    { _id: 'Horror', count: 2 },
                    { _id: 'Sci-fi', count: 1 }
                ],
                tags: [
                    { _id: 'free book', count: 1 },
                    { _id: 'maybe', count: 1 },
                    { _id: 'on kindle', count: 1 },
                    { _id: 'owned', count: 1 },
                    { _id: 'read', count: 1 },
                    { _id: 'Want to read', count: 1 }
                ]
            };

            const { body } = await request.get('/api/filters?genres=Horror')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedFilters);
        });

        it('should return validation error if separator is invalid in the query', async () => {
            const { body } = await request.get('/api/filters?genres=Horror;Romance')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('genres');
        });

        it('should respond with database error', async () => {
            const expectedError = new Error('Database error');
            const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
            const { body } = await request.get('/api/filters')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('stack').and.to.includes('Database error');
            getCollectionStub.restore();
        });
    });

    describe('Ranges', () => {
        it('should return all ranges', async () => {
            const expectedRanges = {
                pages: { max: 781, min: 55 },
                year: { max: 2013, min: 1966 }
            };

            const { body } = await request.get('/api/filters/ranges')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedRanges);
        });

        it('should return only the available ranges based on the request', async () => {
            const expectedRanges = {
                pages: { max: 569, min: 331 },
                year: { max: 2008, min: 1999 }
            };

            const { body } = await request.get('/api/filters/ranges?genres=Sci-fi')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedRanges);
        });

        it('should return validation error if separator is invalid in the query', async () => {
            const { body } = await request.get('/api/filters/ranges?genres=Horror;Romance')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('genres');
        });

        it('should respond with database error', async () => {
            const expectedError = new Error('Database error');
            const getCollectionStub = sinon.stub(db, 'getCollection').throws(expectedError);
            const { body } = await request.get('/api/filters/ranges')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(500);
            expect(body).to.have.property('stack').and.to.includes('Database error');
            getCollectionStub.restore();
        });
    });

    describe('Genres', () => {
        it('should return all genres', async () => {
            const expectedGenres = [
                { name: 'action' },
                { name: 'Adventure' },
                { name: 'Drama' },
                { name: 'Fantasy' },
                { name: 'Horror' },
                { name: 'Romance' },
                { name: 'Sci-fi' },
                { name: 'Thriller' }
            ];

            const { body } = await request.get('/api/filters/genres')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedGenres);
        });

        it('should return all genres with count data', async () => {
            const expectedGenres = [
                { _id: 'action', count: 1 },
                { _id: 'Adventure', count: 1 },
                { _id: 'Drama', count: 1 },
                { _id: 'Fantasy', count: 2 },
                { _id: 'Horror', count: 2 },
                { _id: 'Romance', count: 1 },
                { _id: 'Sci-fi', count: 2 },
                { _id: 'Thriller', count: 2 }
            ];

            const { body } = await request.get('/api/filters/genres/stat')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedGenres);
        });

        it('should return all genres with count data sorted by count', async () => {
            const expectedGenres = [
                { _id: 'Fantasy', count: 2 },
                { _id: 'Horror', count: 2 },
                { _id: 'Sci-fi', count: 2 },
                { _id: 'Thriller', count: 2 },
                { _id: 'action', count: 1 },
                { _id: 'Adventure', count: 1 },
                { _id: 'Drama', count: 1 },
                { _id: 'Romance', count: 1 }
            ];

            const { body } = await request.get('/api/filters/genres/stat?sort=count')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedGenres);
        });

        it('should return validation error if query is invalid', async () => {
            const invalidSorting = 'year';
            const { body } = await request.get(`/api/filters/genres/stat?sort=${invalidSorting}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('sort');
        });
    });

    describe('Tags', () => {
        it('should return all tags', async () => {
            const expectedTags = [
                { name: 'favourite author' },
                { name: 'free book' },
                { name: 'maybe' },
                { name: 'on kindle' },
                { name: 'owned' },
                { name: 'read' },
                { name: 'Want to read' }
            ];

            const { body } = await request.get('/api/filters/tags')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedTags);
        });

        it('should return all tags with count data', async () => {
            const expectedTags = [
                { _id: 'favourite author', count: 2 },
                { _id: 'free book', count: 3 },
                { _id: 'maybe', count: 1 },
                { _id: 'on kindle', count: 3 },
                { _id: 'owned', count: 2 },
                { _id: 'read', count: 3 },
                { _id: 'Want to read', count: 2 }
            ];

            const { body } = await request.get('/api/filters/tags/stat')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedTags);
        });

        it('should return all tags with count data sorted by count', async () => {
            const expectedTags = [
                { _id: 'free book', count: 3 },
                { _id: 'on kindle', count: 3 },
                { _id: 'read', count: 3 },
                { _id: 'favourite author', count: 2 },
                { _id: 'owned', count: 2 },
                { _id: 'Want to read', count: 2 },
                { _id: 'maybe', count: 1 }
            ];

            const { body } = await request.get('/api/filters/tags/stat?sort=count')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedTags);
        });

        it('should return validation error if query is invalid', async () => {
            const invalidSorting = 'year';
            const { body } = await request.get(`/api/filters/tags/stat?sort=${invalidSorting}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('sort');
        });
    });

    describe('Authors', () => {
        it('should return all authors', async () => {
            const expectedAuthors = [
                { name: 'Cassandra Clare' },
                { name: 'Kristin Cashore' },
                { name: 'Stephen King' },
                { name: 'Steven Erikson' }
            ];

            const { body } = await request.get('/api/filters/authors')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedAuthors);
        });

        it('should return all authors with count data', async () => {
            const expectedAuthors = [
                { _id: 'Cassandra Clare', count: 1 },
                { _id: 'Kristin Cashore', count: 2 },
                { _id: 'Stephen King', count: 2 },
                { _id: 'Steven Erikson', count: 2 }
            ];

            const { body } = await request.get('/api/filters/authors/stat')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedAuthors);
        });

        it('should return all authors with count data sorted by count', async () => {
            const expectedAuthors = [
                { _id: 'Kristin Cashore', count: 2 },
                { _id: 'Stephen King', count: 2 },
                { _id: 'Steven Erikson', count: 2 },
                { _id: 'Cassandra Clare', count: 1 }
            ];

            const { body } = await request.get('/api/filters/authors/stat?sort=count')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedAuthors);
        });

        it('should return validation error if query is invalid', async () => {
            const invalidSorting = 'title';
            const { body } = await request.get(`/api/filters/authors/stat?sort=${invalidSorting}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('sort');
        });
    });

    describe('Series', () => {
        it('should return all series', async () => {
            const expectedSeries = [
                { name: 'Above World' },
                { name: 'Graceling Realm' },
                { name: 'Some Quiet Place' }
            ];

            const { body } = await request.get('/api/filters/series')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSeries);
        });

        it('should return all series with count data', async () => {
            const expectedSeries = [
                { _id: 'Above World', count: 1 },
                { _id: 'Graceling Realm', count: 2 },
                { _id: 'Some Quiet Place', count: 2 }
            ];

            const { body } = await request.get('/api/filters/series/stat')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSeries);
        });

        it('should return all series with count data sorted by count', async () => {
            const expectedSeries = [
                { _id: 'Graceling Realm', count: 2 },
                { _id: 'Some Quiet Place', count: 2 },
                { _id: 'Above World', count: 1 }
            ];

            const { body } = await request.get('/api/filters/series/stat?sort=count')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedSeries);
        });

        it('should return validation error if query is invalid', async () => {
            const invalidSorting = 'title';
            const { body } = await request.get(`/api/filters/series/stat?sort=${invalidSorting}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('sort');
        });
    });

    describe('Shelves', () => {
        it('should return all shelves', async () => {
            const expectedShelves = [
                { name: 'Best Book Cover Art' },
                { name: 'Best Historical Fiction' },
                { name: 'Best Horror Novels' },
                { name: 'Best Twists' },
                { name: 'Funny as Hell!' },
                { name: 'The Best Urban Fantasy' },
                { name: 'Thrillers You Must Read!' }
            ];

            const { body } = await request.get('/api/filters/shelves')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedShelves);
        });

        it('should return all shelves with count data', async () => {
            const expectedShelves = [
                { _id: 'Best Book Cover Art', count: 3 },
                { _id: 'Best Historical Fiction', count: 1 },
                { _id: 'Best Horror Novels', count: 2 },
                { _id: 'Best Twists', count: 2 },
                { _id: 'Funny as Hell!', count: 2 },
                { _id: 'The Best Urban Fantasy', count: 1 },
                { _id: 'Thrillers You Must Read!', count: 1 }
            ];

            const { body } = await request.get('/api/filters/shelves/stat')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedShelves);
        });

        it('should return all shelves with count data sorted by count', async () => {
            const expectedShelves = [
                { _id: 'Best Book Cover Art', count: 3 },
                { _id: 'Best Horror Novels', count: 2 },
                { _id: 'Best Twists', count: 2 },
                { _id: 'Funny as Hell!', count: 2 },
                { _id: 'Best Historical Fiction', count: 1 },
                { _id: 'The Best Urban Fantasy', count: 1 },
                { _id: 'Thrillers You Must Read!', count: 1 }
            ];

            const { body } = await request.get('/api/filters/shelves/stat?sort=count')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200);
            expect(body).to.deep.equal(expectedShelves);
        });

        it('should return validation error if query is invalid', async () => {
            const invalidSorting = 'year';
            const { body } = await request.get(`/api/filters/shelves/stat?sort=${invalidSorting}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('sort');
        });

        it('should update the shelf by the given name', async () => {
            const shelfName = 'Best Twists';
            const updatedShelf = {
                shelfName: 'Best Horror Novels'
            };
            const expectedResponse = `Shelf ${shelfName} was successfully renamed!`;
            const { text } = await request.put(`/api/filters/shelves/${shelfName}`)
                .type('json')
                .send(updatedShelf)
                .set('Accept', 'application/json')
                .expect(200);
            expect(text).to.equal(expectedResponse);
        });

        it('should delete the shelf by the given name', async () => {
            const shelfName = 'Best Twists';
            const expectedResponse = `Shelf '${shelfName}' was successfully deleted!`;
            const { text } = await request.delete(`/api/filters/shelves/${shelfName}`)
                .set('Accept', 'application/json')
                .expect(200);
            expect(text).to.equal(expectedResponse);
        });
    });
});
