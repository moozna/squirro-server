const { expect } = require('chai');
const {
    sortParameter,
    uniqueValuesQuery,
    rangeQuery,
    getFirstElement
} = require('./queries');

describe('filters queries', () => {
    describe('sortParameter()', () => {
        it('should return the default sort parameter if sorting is not specified', () => {
            const expectedSortParameter = { $sort: { _id: 1 } };
            const query = sortParameter();
            expect(query).to.deep.equal(expectedSortParameter);
        });

        it('should return the default sort parameter if name is selected', () => {
            const expectedSortParameter = { $sort: { _id: 1 } };
            const query = sortParameter('name');
            expect(query).to.deep.equal(expectedSortParameter);
        });

        it('should return the correct sort parameter if count is selected', () => {
            const expectedSortParameter = { $sort: { count: -1, _id: 1 } };
            const query = sortParameter('count');
            expect(query).to.deep.equal(expectedSortParameter);
        });
    });

    describe('uniqueValuesQuery()', () => {
        it('returns a query with the specified parameters', () => {
            const expectedQuery = [
                { $group: { _id: '$genres' } },
                { $sort: { _id: 1 } },
                { $project: { _id: 0, name: '$_id' } }
            ];
            const query = uniqueValuesQuery('genres');
            expect(query).to.deep.equal(expectedQuery);
        });
    });

    describe('rangeQuery()', () => {
        it('returns a query with the specified fieldName', () => {
            const expectedQuery = [
                {
                    $group: {
                        _id: null,
                        min: { $min: '$publicationYear' },
                        max: { $max: '$publicationYear' }
                    }
                },
                { $project: { _id: 0 } }
            ];
            const query = rangeQuery('publicationYear');
            expect(query).to.deep.equal(expectedQuery);
        });
    });

    describe('getFirstElement()', () => {
        it('returns a query with the specified fieldName', () => {
            const expectedQuery = { $arrayElemAt: ['$pages', 0] };
            const query = getFirstElement('pages');
            expect(query).to.deep.equal(expectedQuery);
        });
    });
});
