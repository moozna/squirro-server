const request = require('supertest');
const express = require('express');
const HTTPStatus = require('http-status');
const fs = require('fs/promises');
const path = require('path');
const defaultHandler = require('./defaultHandler');

const INDEX_HTML_PATH = path.join('public', 'index.html');

async function createIndexHtml() {
    let isFileCreated = false;

    try {
        await fs.writeFile(INDEX_HTML_PATH, '', { flag: 'wx' });
        isFileCreated = true;
    } catch (error) {
        console.log('Index.html already exists', error);
    }

    return isFileCreated;
}

async function removeIndexHtml() {
    await fs.unlink(INDEX_HTML_PATH);
}

describe('defaultHandler()', () => {
    let app;

    const createApp = () => {
        app = express();
        app.get('/api/ok', (req, res) => res.json({ ok: true }));
        app.use(defaultHandler);
    };

    beforeEach(() => {
        createApp();
    });

    it('should not return index.html', async () => {
        await request(app).get('/api/ok').expect(HTTPStatus.OK, { ok: true });
    });

    it('should return index.html', async () => {
        const isFileCreated = await createIndexHtml();

        await request(app)
            .get('/404')
            .expect('Content-Type', /text\/html/)
            .expect(HTTPStatus.OK);

        if (isFileCreated) {
            await removeIndexHtml();
        }
    });

    it('should return 404 when index.html is missing', async () => {
        await request(app).get('/404').expect(404);
    });
});
