function defaultHandler(req, res) {
    res.sendFile('index.html', { root: 'public' }, error => {
        if (error) {
            res.status(error.status).end();
        }
    });
}

module.exports = defaultHandler;
