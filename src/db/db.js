const { MongoClient } = require('mongodb');
const config = require('../../config/config');
const debug = require('debug')('squirro-server:db');

let connection;
let db;

function connect(dbName) {
    return new MongoClient(config.mongodbUri, { useUnifiedTopology: true })
        .connect()
        .then(client => {
            connection = client;
            db = client.db(dbName);
            db.on('close', onClose);
        });
}

function onClose() {
    debug('Connection was closed');
    connection = null;
    db = null;
}

function close() {
    if (connection) {
        return connection.close();
    }
}

function getDb() {
    if (!db) {
        throw new Error('Connection to the database is lost');
    }

    return db;
}

function getCollection(collectionName = 'books') {
    return getDb().collection(collectionName);
}

module.exports = {
    connect,
    close,
    getDb,
    getCollection
};
