const { expect } = require('chai');
const db = require('./db');
const config = require('../../config/config');

describe('database connection', () => {
    afterEach(async () => {
        await db.close();
    });

    it('should throw an error if connection to the db is not available', async () => {
        expect(db.getDb).to.throw('Connection to the database is lost');
    });

    it('should return a mongo error if connection fails', async () => {
        const nonExistentDb = 'not-exist';
        try {
            await db.connect(nonExistentDb);
        } catch (err) {
            expect(err.name).to.equal('MongoError');
        }
    });

    describe('getters', () => {
        beforeEach(async () => {
            await db.connect(config.dbName.test);
        });

        afterEach(async () => {
            await db.close();
        });

        it('should return the connected db instance', async () => {
            const dbInstance = db.getDb();
            expect(dbInstance).to.be.an.instanceOf(Object);
            expect(dbInstance.databaseName).to.be.equal('squirro-mock-test');
        });

        it('should return the specified collection', async () => {
            const collection = db.getCollection('test');
            expect(collection).to.be.an.instanceOf(Object);
            expect(collection.collectionName).to.be.equal('test');
        });

        it('should return the default collection if not specified', async () => {
            const collection = db.getCollection();
            expect(collection).to.be.an.instanceOf(Object);
            expect(collection.collectionName).to.be.equal('books');
        });
    });
});
