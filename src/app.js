const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const path = require('path');

const home = require('./routes/home');
const books = require('./routes/books');
const filters = require('./routes/filters');
const images = require('./routes/images');
const notFoundHandler = require('./routes/notFound/notFoundHandler');
const defaultHandler = require('./routes/default/defaultHandler');
const serverErrorHandler = require('./middlewares/serverErrorHandler');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '..', 'public')));

app.use('/api/home', home);
app.use('/api/books', books);
app.use('/api/filters', filters);
app.use('/api/images', images);

app.use('/api', notFoundHandler);
app.use(defaultHandler);
app.use(serverErrorHandler);

module.exports = app;
