#!/usr/bin/env node

const app = require('../src/app');
const debug = require('debug')('squirro-server:server');
const http = require('http');
const config = require('../config/config');
const db = require('./db/db');

const port = normalizePort(process.env.PORT || config.expressPort);
app.set('port', port);

const server = http.createServer(app);

db.connect(config.dbName.mock)
    .then(() => {
        debug('Connected to the database...');
        // Listen on provided port, on all network interfaces.
        server.listen(port);
        server.on('error', onError);
        server.on('listening', onListening);
    })
    .catch(error => {
        debug('Connecting to the database failed', error);
        debug('App closes');
        process.exit(1);
    });


// Normalize a port into a number, string, or false.
function normalizePort(val) {
    const portNumber = parseInt(val, 10);

    if (Number.isNaN(portNumber)) {
        // named pipe
        return val;
    }

    if (portNumber >= 0) {
        return portNumber;
    }

    return false;
}

// Event listener for HTTP server "error" event.
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? `Pipe ${port}`
        : `Port ${port}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

// Event listener for HTTP server "listening" event.
function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? `pipe ${addr}`
        : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
}
