const { expect } = require('chai');
const isObject = require('./isObject');

describe('isObject()', () => {
    it('is a function', () => {
        expect(typeof isObject).to.equal('function');
    });

    describe('returns true', () => {
        it('when value is an object', () => {
            const testValue = {};
            expect(isObject(testValue)).to.be.ok;
        });
    });

    describe('returns false', () => {
        it('when value is undefined', () => {
            const testValue = undefined;
            expect(isObject(testValue)).to.be.not.ok;
        });

        it('when value is null', () => {
            const testValue = null;
            expect(isObject(testValue)).to.be.not.ok;
        });

        it('when value is array', () => {
            const testValue = [];
            expect(isObject(testValue)).to.be.not.ok;
        });
    });
});
