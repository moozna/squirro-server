const { expect } = require('chai');
const sinon = require('sinon');
const fs = require('fs');
const {
    copyFile,
    renameFile,
    deleteFile,
    getFullFilePath
} = require('./fileUtils');

describe('fileUtils', () => {
    const expectedError = 'Error';

    describe('copyFile()', () => {
        const sourcePath = 'sourcePath';
        const targetPath = 'targetPath';

        it('is a function', () => {
            expect(typeof copyFile).to.equal('function');
        });

        it('resolves a promise', done => {
            const copyFileStub = sinon
                .stub(fs, 'copyFile')
                .callsFake((sourcePath, targetPath, callback) => callback());

            copyFile(sourcePath, targetPath).then(done);
            copyFileStub.restore();
        });

        it('rejects a promise with an error', async () => {
            const copyFileStub = sinon
                .stub(fs, 'copyFile')
                .callsFake((sourcePath, targetPath, callback) =>
                    callback(expectedError)
                );

            try {
                await copyFile(sourcePath, targetPath);
            } catch (error) {
                expect(error).to.equal(expectedError);
            }
            copyFileStub.restore();
        });
    });

    describe('renameFile()', () => {
        const currentPath = 'currentPath';
        const targetPath = 'targetPath';

        it('is a function', () => {
            expect(typeof renameFile).to.equal('function');
        });

        it('resolves a promise', done => {
            const renameFileStub = sinon
                .stub(fs, 'rename')
                .callsFake((currentPath, targetPath, callback) => callback());

            renameFile(currentPath, targetPath).then(done);
            renameFileStub.restore();
        });

        it('rejects a promise with an error', async () => {
            const renameFileStub = sinon
                .stub(fs, 'rename')
                .callsFake((currentPath, targetPath, callback) =>
                    callback(expectedError)
                );

            try {
                await renameFile(currentPath, targetPath);
            } catch (error) {
                expect(error).to.equal(expectedError);
            }
            renameFileStub.restore();
        });
    });

    describe('deleteFile()', () => {
        const filePath = 'filePath';

        it('is a function', () => {
            expect(typeof deleteFile).to.equal('function');
        });

        it('resolves a promise', done => {
            const deleteFileStub = sinon
                .stub(fs, 'unlink')
                .callsFake((filePath, callback) => callback());

            deleteFile(filePath).then(done);
            deleteFileStub.restore();
        });

        it('rejects a promise with an error', async () => {
            const deleteFileStub = sinon
                .stub(fs, 'unlink')
                .callsFake((filePath, callback) => callback(expectedError));

            try {
                await deleteFile(filePath);
            } catch (error) {
                expect(error).to.equal(expectedError);
            }
            deleteFileStub.restore();
        });
    });

    describe('getFullFilePath()', () => {
        const filePath = '/folder/filePath';

        it('is a function', () => {
            expect(typeof getFullFilePath).to.equal('function');
        });

        it('returns the filepath from public folder', () => {
            const expectedPath = `./public${filePath}`
            expect(getFullFilePath(filePath)).to.equal(expectedPath);
        });

        it('returns the filepath from the specified folder', () => {
            const baseFolder = './tests';
            const expectedPath = `${baseFolder}${filePath}`
            expect(getFullFilePath(filePath, baseFolder)).to.equal(expectedPath);
        });
    });
});
