const set = (object, path = '', value = undefined) => {
    const pathElements = path.split('.');
    const length = pathElements.length;
    const lastIndex = length - 1;

    if (!object || !path || !pathElements[lastIndex]) {
        return object;
    }

    let partialObj = object;

    pathElements.forEach((item, index) => {
        const key = pathElements[index];

        if (index === lastIndex) {
            partialObj[key] = value;
        } else {
            const currentValue = partialObj[key];
            if (!currentValue) {
                partialObj[key] = {};
            }
            partialObj = partialObj[key];
        }
    });

    return object;
};

module.exports = set;
