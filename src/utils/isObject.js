const isObject = value =>
    value && typeof value === 'object' && !Array.isArray(value);

module.exports = isObject;
