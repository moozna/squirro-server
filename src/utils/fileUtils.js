const fs = require('fs');

function copyFile(sourcePath, targetPath) {
    return new Promise((resolve, reject) => {
        fs.copyFile(sourcePath, targetPath, error => {
            if (error) {
                reject(error);
            }
            resolve();
        });
    });
}

function renameFile(currentPath, targetPath) {
    return new Promise((resolve, reject) => {
        fs.rename(currentPath, targetPath, error => {
            if (error) {
                reject(error);
            }
            resolve();
        });
    });
}

function deleteFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, error => {
            if (error) {
                return reject(error);
            }
            resolve();
        });
    });
}

function getFullFilePath(filePath, basePath = './public') {
    return `${basePath}${filePath}`;
}

module.exports = {
    copyFile,
    renameFile,
    deleteFile,
    getFullFilePath
};
