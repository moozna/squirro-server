const { expect } = require('chai');
const set = require('./set');

describe('set()', () => {
    it('is a function', () => {
        expect(typeof set).to.equal('function');
    });

    describe('does not throw', () => {
        it('when object is undefined', () => {
            const testObj = undefined;
            expect(set(testObj, 'something')).to.equal(undefined);
        });

        it('when object is null', () => {
            const testObj = null;
            expect(set(testObj, 'something')).to.equal(null);
        });

        it('when path is not provided', () => {
            const testObj = {};
            expect(set(testObj)).to.deep.equal({});
        });

        it('when incorrect path is provided', () => {
            const testObj = {};
            expect(set(testObj, 'something.other.', 42)).to.deep.equal({});
        });
    });

    describe('assigns the value', () => {
        it('when the property exists', () => {
            const testObj = {
                title: 'title'
            };
            const expected = {
                title: 'Some title'
            };

            expect(set(testObj, 'title', 'Some title')).to.deep.equal(expected);
        });

        it('when the property does not exist', () => {
            const testObj = {};
            const expected = {
                title: 'Some title'
            };

            expect(set(testObj, 'title', 'Some title')).to.deep.equal(expected);
        });

        it('when nested property provided', () => {
            const testObj = {
                rating: {
                    amazonRating: 5
                }
            };
            const expected = {
                rating: {
                    amazonRating: 3.1
                }
            };

            expect(set(testObj, 'rating.amazonRating', 3.1)).to.deep.equal(
                expected
            );
        });

        it('when nested property does not exist', () => {
            const testObj = {};
            const expected = {
                rating: {
                    amazonRating: 3.1
                }
            };

            expect(set(testObj, 'rating.amazonRating', 3.1)).to.deep.equal(
                expected
            );
        });
    });
});
