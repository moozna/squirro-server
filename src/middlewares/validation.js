function validateParams(schema) {
    return validation('params', schema);
}

function validateBody(schema) {
    return validation('body', schema);
}

function validateQuery(schema) {
    return validation('query', schema);
}

function validation(key, schema) {
    return (req, res, next) => {
        if (!schema) {
            return next();
        }

        const { error, value } = schema.validate(req[key]);

        if (error) {
            const title = 'ValidationError';
            const message = error.details
                .map(err => `${err.message} at ${err.path}`)
                .join(', ');
            const source = error.details.map(err => err.context.key).join(', ');
            res.status(422).json({ title, message, source }).end();
        } else {
            req[key] = value;
            next();
        }
    };
}

module.exports = {
    validateParams,
    validateBody,
    validateQuery
};
