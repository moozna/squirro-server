const { expect } = require('chai');
const request = require('supertest');
const express = require('express');
const asyncErrorHandler = require('./asyncErrorHandler');

function makeResult(result) {
    return new Promise(resolve => {
        setTimeout(() => resolve(result), 10);
    });
}

describe('asyncErrorHandler()', () => {
    it('should await any promises', async () => {
        const app = express();
        app.get('/', asyncErrorHandler(async (req, res) => {
            const results = [];

            for (let i = 0; i < 5; i++) {
                results.push(makeResult(`test${i}`));
            }

            res.send((await Promise.all(results)).join());
        }));

        const { text } = await request(app)
            .get('/')
            .expect(200);
        expect(text).to.equal('test0,test1,test2,test3,test4');
    });


    it('should propagate route errors to error handler', async () => {
        const app = express();
        app.get('/error', asyncErrorHandler(async () => {
            throw new Error('error');
        }));

        // eslint-disable-next-line no-unused-vars
        app.use((err, req, res, next) => {
            res.status(500).send('error');
        });

        const { text } = await request(app)
            .get('/error')
            .expect(500);
        expect(text).to.equal('error');
    });
});
