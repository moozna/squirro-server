const { expect } = require('chai');
const request = require('supertest');
const express = require('express');
const HTTPStatus = require('http-status');
const HTTPError = require('http-errors');
const serverErrorHandler = require('./serverErrorHandler');

describe('serverErrorHandler()', () => {
    let app;

    const createApp = () => {
        app = express();
        app.get('/ok', (req, res) => res.json({ ok: true }));
        app.get('/internal', (req, res, next) => next(new Error('custom error')));
        app.get('/http-error', (req, res, next) => next(new HTTPError.NotFound('Object not found')));
        app.get('/invalid-code', (req, res, next) => {
            const err = new Error('invalid-code');
            err.statusCode = 300;
            next(err);
        });
        app.use(serverErrorHandler);
    };

    describe('development mode', () => {
        beforeEach(() => {
            process.env.NODE_ENV = 'development';
            createApp();
        });

        it('should not return an error', async () => {
            await request(app)
                .get('/ok')
                .expect(HTTPStatus.OK, { ok: true });
        });

        it('should return an internal error', async () => {
            const { body } = await request(app)
                .get('/internal')
                .expect(HTTPStatus.INTERNAL_SERVER_ERROR);
            expect(body).to.have.property('message').and.equal('custom error');
            expect(body).to.have.property('status').and.equal(HTTPStatus.INTERNAL_SERVER_ERROR);
            expect(body).to.have.property('stack').and.match(/Error: custom error/);
        });

        it('should fix an http code if < 400', async () => {
            const { body } = await request(app)
                .get('/invalid-code')
                .expect(HTTPStatus.BAD_REQUEST);
            expect(body).to.have.property('message').and.equal('invalid-code');
            expect(body).to.have.property('status').and.equal(HTTPStatus.BAD_REQUEST);
        });

        it('should return an error from http-errors', async () => {
            const { body } = await request(app)
                .get('/http-error')
                .expect(HTTPStatus.NOT_FOUND);
            expect(body).to.have.property('message').and.equal('Object not found');
            expect(body).to.have.property('status').and.equal(HTTPStatus.NOT_FOUND);
        });
    });

    describe('production mode', () => {
        beforeEach(() => {
            process.env.NODE_ENV = 'production';
            createApp();
        });

        afterEach(() => {
            process.env.NODE_ENV = undefined;
            createApp();
        });

        it('should not return a call stack in production mode', async () => {
            const { body } = await request(app)
                .get('/internal')
                .expect(HTTPStatus.INTERNAL_SERVER_ERROR);
            expect(body).to.not.have.property('stack');
        });
    });
});
