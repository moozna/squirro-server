const HTTPStatus = require('http-status');
const debug = require('debug')('squirro-server:errorhandler');

function serverErrorHandler(err, req, res, next) { // eslint-disable-line no-unused-vars
    let status = err.status || err.statusCode || HTTPStatus.INTERNAL_SERVER_ERROR;

    if (status < HTTPStatus.BAD_REQUEST) {
        status = HTTPStatus.BAD_REQUEST;
    }
    const body = {
        message: err.message,
        status
    };

    if (process.env.NODE_ENV !== 'production') {
        body.stack = err.stack;
    }

    debug(err);
    res.status(status);
    res.json(body);
}

module.exports = serverErrorHandler;
