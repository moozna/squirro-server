const { expect } = require('chai');
const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const Joi = require('joi');
const { validateQuery, validateParams, validateBody } = require('./validation');

const booksQuerySchema = Joi.object({
    limit: Joi.number().required()
});

const booksIdSchema = Joi.object({
    id: Joi.number().required()
});

const authorsSchema = Joi.object({
    firstName: Joi.string().required(),
    email: Joi.string().email().required()
});

describe('validation()', () => {
    let app;

    const createApp = () => {
        app = express();
        app.use(bodyParser.json());
        app.get('/books', validateQuery(booksQuerySchema), (req, res) => res.send('ok'));
        app.get('/books/:id', validateParams(booksIdSchema), (req, res) => res.send('ok'));
        app.post('/authors', validateBody(authorsSchema), (req, res) => res.send('ok'));
        app.get('/statistic', validateBody(), (req, res) => res.send('ok'));
    };

    createApp();

    describe('query validation', () => {
        it('should return 422 for failed query validation', async () => {
            await request(app)
                .get('/books')
                .expect(422);
        });

        it('should return 200 for successful query validation', async () => {
            await request(app)
                .get('/books?limit=10')
                .expect(200, 'ok');
        });
    });

    describe('params validation', () => {
        it('should return 422 for failed params validation', async () => {
            await request(app)
                .get('/books/godzilla')
                .expect(422);
        });

        it('should return 200 for successful params validation', async () => {
            await request(app)
                .get('/books/3')
                .expect(200, 'ok');
        });
    });

    describe('body validation', () => {
        it('should return 422 for failed body validation', async () => {
            const { body } = await request(app)
                .post('/authors')
                .send({ firstName: 'Jennifer' })
                .expect(422);
            expect(body).to.have.property('title').and.equal('ValidationError');
            expect(body).to.have.property('source').and.equal('email');
        });

        it('should return 200 for successful body validation', async () => {
            await request(app)
                .post('/authors')
                .send({ firstName: 'Jennifer', email: 'info@jenniferlarmentrout.com' })
                .expect(200, 'ok');
        });
    });

    describe('without validation', () => {
        it('should skip validation if no schema provided', async () => {
            await request(app)
                .get('/statistic')
                .expect(200, 'ok');
        });
    });
});
