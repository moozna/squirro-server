# Squirro-server #

Run the application:

    `npm start`

Run the application in watch mode

    `npm run devstart`

Run testing with coverage report

    `npm test`

Run testing in watch mode

    `npm run tdd`
